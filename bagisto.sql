/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : bagisto

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-10-02 16:19:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `role_id` int(10) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES ('1', 'Example', 'admin@example.com', '$2y$10$G3REXLxcK78bNT0P8Bxz/ORQNG7BjOET08z2XznOMlCMZrgoKPsLK', '1', '1', 'rCgGRQ5FRrgBAJLuD5JPGmt2o39JoWo1ONaPnDepr2THRI6n6bfRHYORm1bY', null, null);

-- ----------------------------
-- Table structure for admin_password_resets
-- ----------------------------
DROP TABLE IF EXISTS `admin_password_resets`;
CREATE TABLE `admin_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `admin_password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of admin_password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for attributes
-- ----------------------------
DROP TABLE IF EXISTS `attributes`;
CREATE TABLE `attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `validation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `is_required` tinyint(1) NOT NULL DEFAULT 0,
  `is_unique` tinyint(1) NOT NULL DEFAULT 0,
  `value_per_locale` tinyint(1) NOT NULL DEFAULT 0,
  `value_per_channel` tinyint(1) NOT NULL DEFAULT 0,
  `is_filterable` tinyint(1) NOT NULL DEFAULT 0,
  `is_configurable` tinyint(1) NOT NULL DEFAULT 0,
  `is_user_defined` tinyint(1) NOT NULL DEFAULT 1,
  `is_visible_on_front` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `swatch_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `use_in_flat` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attributes_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of attributes
-- ----------------------------
INSERT INTO `attributes` VALUES ('1', 'sku', 'SKU', 'text', null, '1', '1', '1', '0', '0', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('2', 'name', 'Name', 'text', null, '2', '1', '0', '1', '1', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('3', 'url_key', 'URL Key', 'text', null, '3', '1', '1', '0', '0', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('4', 'tax_category_id', 'Tax Category', 'select', null, '4', '0', '0', '0', '1', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('5', 'new', 'New', 'boolean', null, '5', '0', '0', '0', '0', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('6', 'featured', 'Featured', 'boolean', null, '6', '0', '0', '0', '0', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('7', 'visible_individually', 'Visible Individually', 'boolean', null, '7', '1', '0', '0', '0', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('8', 'status', 'Status', 'boolean', null, '8', '1', '0', '0', '0', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('9', 'short_description', 'Short Description', 'textarea', null, '9', '1', '0', '1', '1', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('10', 'description', 'Description', 'textarea', null, '10', '1', '0', '1', '1', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('11', 'price', 'Price', 'price', 'decimal', '11', '1', '0', '0', '0', '1', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('12', 'cost', 'Cost', 'price', 'decimal', '12', '0', '0', '0', '1', '0', '0', '1', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('13', 'special_price', 'Special Price', 'price', 'decimal', '13', '0', '0', '0', '0', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('14', 'special_price_from', 'Special Price From', 'date', null, '14', '0', '0', '0', '1', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('15', 'special_price_to', 'Special Price To', 'date', null, '15', '0', '0', '0', '1', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('16', 'meta_title', 'Meta Title', 'textarea', null, '16', '0', '0', '1', '1', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('17', 'meta_keywords', 'Meta Keywords', 'textarea', null, '17', '0', '0', '1', '1', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('18', 'meta_description', 'Meta Description', 'textarea', null, '18', '0', '0', '1', '1', '0', '0', '1', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('19', 'width', 'Width', 'text', 'decimal', '19', '0', '0', '0', '0', '0', '0', '1', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('20', 'height', 'Height', 'text', 'decimal', '20', '0', '0', '0', '0', '0', '0', '1', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('21', 'depth', 'Depth', 'text', 'decimal', '21', '0', '0', '0', '0', '0', '0', '1', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('22', 'weight', 'Weight', 'text', 'decimal', '22', '1', '0', '0', '0', '0', '0', '0', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('23', 'color', 'Color', 'select', null, '23', '0', '0', '0', '0', '1', '1', '1', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('24', 'size', 'Size', 'select', null, '24', '0', '0', '0', '0', '1', '1', '1', '0', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');
INSERT INTO `attributes` VALUES ('25', 'brand', 'Brand', 'select', null, '25', '0', '0', '0', '0', '1', '0', '0', '1', '2019-10-02 14:15:30', '2019-10-02 14:15:30', null, '1');

-- ----------------------------
-- Table structure for attribute_families
-- ----------------------------
DROP TABLE IF EXISTS `attribute_families`;
CREATE TABLE `attribute_families` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `is_user_defined` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of attribute_families
-- ----------------------------
INSERT INTO `attribute_families` VALUES ('1', 'default', 'Default', '0', '1');

-- ----------------------------
-- Table structure for attribute_groups
-- ----------------------------
DROP TABLE IF EXISTS `attribute_groups`;
CREATE TABLE `attribute_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `is_user_defined` tinyint(1) NOT NULL DEFAULT 1,
  `attribute_family_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attribute_groups_attribute_family_id_name_unique` (`attribute_family_id`,`name`),
  CONSTRAINT `attribute_groups_attribute_family_id_foreign` FOREIGN KEY (`attribute_family_id`) REFERENCES `attribute_families` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of attribute_groups
-- ----------------------------
INSERT INTO `attribute_groups` VALUES ('1', 'General', '1', '0', '1');
INSERT INTO `attribute_groups` VALUES ('2', 'Description', '2', '0', '1');
INSERT INTO `attribute_groups` VALUES ('3', 'Meta Description', '3', '0', '1');
INSERT INTO `attribute_groups` VALUES ('4', 'Price', '4', '0', '1');
INSERT INTO `attribute_groups` VALUES ('5', 'Shipping', '5', '0', '1');

-- ----------------------------
-- Table structure for attribute_group_mappings
-- ----------------------------
DROP TABLE IF EXISTS `attribute_group_mappings`;
CREATE TABLE `attribute_group_mappings` (
  `attribute_id` int(10) unsigned NOT NULL,
  `attribute_group_id` int(10) unsigned NOT NULL,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`attribute_id`,`attribute_group_id`),
  KEY `attribute_group_mappings_attribute_group_id_foreign` (`attribute_group_id`),
  CONSTRAINT `attribute_group_mappings_attribute_group_id_foreign` FOREIGN KEY (`attribute_group_id`) REFERENCES `attribute_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `attribute_group_mappings_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of attribute_group_mappings
-- ----------------------------
INSERT INTO `attribute_group_mappings` VALUES ('1', '1', '1');
INSERT INTO `attribute_group_mappings` VALUES ('2', '1', '2');
INSERT INTO `attribute_group_mappings` VALUES ('3', '1', '3');
INSERT INTO `attribute_group_mappings` VALUES ('4', '1', '4');
INSERT INTO `attribute_group_mappings` VALUES ('5', '1', '5');
INSERT INTO `attribute_group_mappings` VALUES ('6', '1', '6');
INSERT INTO `attribute_group_mappings` VALUES ('7', '1', '7');
INSERT INTO `attribute_group_mappings` VALUES ('8', '1', '8');
INSERT INTO `attribute_group_mappings` VALUES ('9', '2', '1');
INSERT INTO `attribute_group_mappings` VALUES ('10', '2', '2');
INSERT INTO `attribute_group_mappings` VALUES ('11', '4', '1');
INSERT INTO `attribute_group_mappings` VALUES ('12', '4', '2');
INSERT INTO `attribute_group_mappings` VALUES ('13', '4', '3');
INSERT INTO `attribute_group_mappings` VALUES ('14', '4', '4');
INSERT INTO `attribute_group_mappings` VALUES ('15', '4', '5');
INSERT INTO `attribute_group_mappings` VALUES ('16', '3', '1');
INSERT INTO `attribute_group_mappings` VALUES ('17', '3', '2');
INSERT INTO `attribute_group_mappings` VALUES ('18', '3', '3');
INSERT INTO `attribute_group_mappings` VALUES ('19', '5', '1');
INSERT INTO `attribute_group_mappings` VALUES ('20', '5', '2');
INSERT INTO `attribute_group_mappings` VALUES ('21', '5', '3');
INSERT INTO `attribute_group_mappings` VALUES ('22', '5', '4');
INSERT INTO `attribute_group_mappings` VALUES ('23', '1', '9');
INSERT INTO `attribute_group_mappings` VALUES ('24', '1', '10');
INSERT INTO `attribute_group_mappings` VALUES ('25', '1', '11');

-- ----------------------------
-- Table structure for attribute_options
-- ----------------------------
DROP TABLE IF EXISTS `attribute_options`;
CREATE TABLE `attribute_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  `swatch_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attribute_options_attribute_id_foreign` (`attribute_id`),
  CONSTRAINT `attribute_options_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of attribute_options
-- ----------------------------
INSERT INTO `attribute_options` VALUES ('1', 'Red', '1', '23', null);
INSERT INTO `attribute_options` VALUES ('2', 'Green', '2', '23', null);
INSERT INTO `attribute_options` VALUES ('3', 'Yellow', '3', '23', null);
INSERT INTO `attribute_options` VALUES ('4', 'Black', '4', '23', null);
INSERT INTO `attribute_options` VALUES ('5', 'White', '5', '23', null);
INSERT INTO `attribute_options` VALUES ('6', 'S', '1', '24', null);
INSERT INTO `attribute_options` VALUES ('7', 'M', '2', '24', null);
INSERT INTO `attribute_options` VALUES ('8', 'L', '3', '24', null);
INSERT INTO `attribute_options` VALUES ('9', 'XL', '4', '24', null);

-- ----------------------------
-- Table structure for attribute_option_translations
-- ----------------------------
DROP TABLE IF EXISTS `attribute_option_translations`;
CREATE TABLE `attribute_option_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_option_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attribute_option_translations_attribute_option_id_locale_unique` (`attribute_option_id`,`locale`),
  CONSTRAINT `attribute_option_translations_attribute_option_id_foreign` FOREIGN KEY (`attribute_option_id`) REFERENCES `attribute_options` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of attribute_option_translations
-- ----------------------------
INSERT INTO `attribute_option_translations` VALUES ('1', 'en', 'Red', '1');
INSERT INTO `attribute_option_translations` VALUES ('2', 'en', 'Green', '2');
INSERT INTO `attribute_option_translations` VALUES ('3', 'en', 'Yellow', '3');
INSERT INTO `attribute_option_translations` VALUES ('4', 'en', 'Black', '4');
INSERT INTO `attribute_option_translations` VALUES ('5', 'en', 'White', '5');
INSERT INTO `attribute_option_translations` VALUES ('6', 'en', 'S', '6');
INSERT INTO `attribute_option_translations` VALUES ('7', 'en', 'M', '7');
INSERT INTO `attribute_option_translations` VALUES ('8', 'en', 'L', '8');
INSERT INTO `attribute_option_translations` VALUES ('9', 'en', 'XL', '9');

-- ----------------------------
-- Table structure for attribute_translations
-- ----------------------------
DROP TABLE IF EXISTS `attribute_translations`;
CREATE TABLE `attribute_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attribute_translations_attribute_id_locale_unique` (`attribute_id`,`locale`),
  CONSTRAINT `attribute_translations_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of attribute_translations
-- ----------------------------
INSERT INTO `attribute_translations` VALUES ('1', 'en', 'SKU', '1');
INSERT INTO `attribute_translations` VALUES ('2', 'en', 'Name', '2');
INSERT INTO `attribute_translations` VALUES ('3', 'en', 'URL Key', '3');
INSERT INTO `attribute_translations` VALUES ('4', 'en', 'Tax Category', '4');
INSERT INTO `attribute_translations` VALUES ('5', 'en', 'New', '5');
INSERT INTO `attribute_translations` VALUES ('6', 'en', 'Featured', '6');
INSERT INTO `attribute_translations` VALUES ('7', 'en', 'Visible Individually', '7');
INSERT INTO `attribute_translations` VALUES ('8', 'en', 'Status', '8');
INSERT INTO `attribute_translations` VALUES ('9', 'en', 'Short Description', '9');
INSERT INTO `attribute_translations` VALUES ('10', 'en', 'Description', '10');
INSERT INTO `attribute_translations` VALUES ('11', 'en', 'Price', '11');
INSERT INTO `attribute_translations` VALUES ('12', 'en', 'Cost', '12');
INSERT INTO `attribute_translations` VALUES ('13', 'en', 'Special Price', '13');
INSERT INTO `attribute_translations` VALUES ('14', 'en', 'Special Price From', '14');
INSERT INTO `attribute_translations` VALUES ('15', 'en', 'Special Price To', '15');
INSERT INTO `attribute_translations` VALUES ('16', 'en', 'Meta Description', '16');
INSERT INTO `attribute_translations` VALUES ('17', 'en', 'Meta Keywords', '17');
INSERT INTO `attribute_translations` VALUES ('18', 'en', 'Meta Description', '18');
INSERT INTO `attribute_translations` VALUES ('19', 'en', 'Width', '19');
INSERT INTO `attribute_translations` VALUES ('20', 'en', 'Height', '20');
INSERT INTO `attribute_translations` VALUES ('21', 'en', 'Depth', '21');
INSERT INTO `attribute_translations` VALUES ('22', 'en', 'Weight', '22');
INSERT INTO `attribute_translations` VALUES ('23', 'en', 'Color', '23');
INSERT INTO `attribute_translations` VALUES ('24', 'en', 'Size', '24');
INSERT INTO `attribute_translations` VALUES ('25', 'en', 'Brand', '25');

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_gift` tinyint(1) NOT NULL DEFAULT 0,
  `items_count` int(11) DEFAULT NULL,
  `items_qty` decimal(12,4) DEFAULT NULL,
  `exchange_rate` decimal(12,4) DEFAULT NULL,
  `global_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `base_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cart_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grand_total` decimal(12,4) DEFAULT 0.0000,
  `base_grand_total` decimal(12,4) DEFAULT 0.0000,
  `sub_total` decimal(12,4) DEFAULT 0.0000,
  `base_sub_total` decimal(12,4) DEFAULT 0.0000,
  `tax_total` decimal(12,4) DEFAULT 0.0000,
  `base_tax_total` decimal(12,4) DEFAULT 0.0000,
  `discount_amount` decimal(12,4) DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) DEFAULT 0.0000,
  `checkout_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_guest` tinyint(1) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT 1,
  `conversion_time` datetime DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `channel_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_customer_id_foreign` (`customer_id`),
  KEY `cart_channel_id_foreign` (`channel_id`),
  CONSTRAINT `cart_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cart_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart
-- ----------------------------

-- ----------------------------
-- Table structure for cart_address
-- ----------------------------
DROP TABLE IF EXISTS `cart_address`;
CREATE TABLE `cart_address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cart_id` int(10) unsigned DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_address_cart_id_foreign` (`cart_id`),
  KEY `cart_address_customer_id_foreign` (`customer_id`),
  CONSTRAINT `cart_address_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cart_address_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart_address
-- ----------------------------

-- ----------------------------
-- Table structure for cart_items
-- ----------------------------
DROP TABLE IF EXISTS `cart_items`;
CREATE TABLE `cart_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quantity` int(10) unsigned NOT NULL DEFAULT 0,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` decimal(12,4) NOT NULL DEFAULT 1.0000,
  `total_weight` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_total_weight` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `price` decimal(12,4) NOT NULL DEFAULT 1.0000,
  `base_price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `total` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_total` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `tax_percent` decimal(12,4) DEFAULT 0.0000,
  `tax_amount` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount` decimal(12,4) DEFAULT 0.0000,
  `discount_percent` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `discount_amount` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `additional` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `cart_id` int(10) unsigned NOT NULL,
  `tax_category_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `custom_price` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_items_product_id_foreign` (`product_id`),
  KEY `cart_items_cart_id_foreign` (`cart_id`),
  KEY `cart_items_tax_category_id_foreign` (`tax_category_id`),
  KEY `cart_items_parent_id_foreign` (`parent_id`),
  CONSTRAINT `cart_items_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cart_items_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `cart_items` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cart_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cart_items_tax_category_id_foreign` FOREIGN KEY (`tax_category_id`) REFERENCES `tax_categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart_items
-- ----------------------------

-- ----------------------------
-- Table structure for cart_item_inventories
-- ----------------------------
DROP TABLE IF EXISTS `cart_item_inventories`;
CREATE TABLE `cart_item_inventories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qty` int(10) unsigned NOT NULL DEFAULT 0,
  `inventory_source_id` int(10) unsigned DEFAULT NULL,
  `cart_item_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart_item_inventories
-- ----------------------------

-- ----------------------------
-- Table structure for cart_payment
-- ----------------------------
DROP TABLE IF EXISTS `cart_payment`;
CREATE TABLE `cart_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cart_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_payment_cart_id_foreign` (`cart_id`),
  CONSTRAINT `cart_payment_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart_payment
-- ----------------------------

-- ----------------------------
-- Table structure for cart_rules
-- ----------------------------
DROP TABLE IF EXISTS `cart_rules`;
CREATE TABLE `cart_rules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `starts_from` datetime DEFAULT NULL,
  `ends_till` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `per_customer` int(10) unsigned NOT NULL DEFAULT 0,
  `use_coupon` tinyint(1) NOT NULL DEFAULT 0,
  `usage_limit` int(10) unsigned NOT NULL DEFAULT 0,
  `conditions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `actions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `end_other_rules` tinyint(1) NOT NULL DEFAULT 0,
  `priority` int(10) unsigned NOT NULL DEFAULT 0,
  `uses_attribute_conditions` tinyint(1) NOT NULL DEFAULT 0,
  `product_ids` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_order` int(10) unsigned NOT NULL DEFAULT 0,
  `action_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disc_amount` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `disc_quantity` decimal(12,4) NOT NULL DEFAULT 1.0000,
  `disc_threshold` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `coupon_type` int(11) NOT NULL DEFAULT 1,
  `auto_generation` tinyint(1) NOT NULL DEFAULT 0,
  `apply_to_shipping` tinyint(1) NOT NULL DEFAULT 0,
  `free_shipping` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `products_selection` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart_rules
-- ----------------------------

-- ----------------------------
-- Table structure for cart_rule_cart
-- ----------------------------
DROP TABLE IF EXISTS `cart_rule_cart`;
CREATE TABLE `cart_rule_cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart_rule_id` int(10) unsigned DEFAULT NULL,
  `cart_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_rule_cart_cart_rule_id_foreign` (`cart_rule_id`),
  KEY `cart_rule_cart_cart_id_foreign` (`cart_id`),
  CONSTRAINT `cart_rule_cart_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cart_rule_cart_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart_rule_cart
-- ----------------------------

-- ----------------------------
-- Table structure for cart_rule_channels
-- ----------------------------
DROP TABLE IF EXISTS `cart_rule_channels`;
CREATE TABLE `cart_rule_channels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart_rule_id` int(10) unsigned NOT NULL,
  `channel_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_rule_channels_cart_rule_id_foreign` (`cart_rule_id`),
  KEY `cart_rule_channels_channel_id_foreign` (`channel_id`),
  CONSTRAINT `cart_rule_channels_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cart_rule_channels_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart_rule_channels
-- ----------------------------

-- ----------------------------
-- Table structure for cart_rule_coupons
-- ----------------------------
DROP TABLE IF EXISTS `cart_rule_coupons`;
CREATE TABLE `cart_rule_coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart_rule_id` int(10) unsigned NOT NULL,
  `prefix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suffix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usage_limit` int(10) unsigned NOT NULL DEFAULT 0,
  `usage_per_customer` int(10) unsigned NOT NULL DEFAULT 0,
  `usage_throttle` int(10) unsigned NOT NULL DEFAULT 0,
  `type` int(10) unsigned NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_rule_coupons_cart_rule_id_foreign` (`cart_rule_id`),
  CONSTRAINT `cart_rule_coupons_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart_rule_coupons
-- ----------------------------

-- ----------------------------
-- Table structure for cart_rule_coupons_usage
-- ----------------------------
DROP TABLE IF EXISTS `cart_rule_coupons_usage`;
CREATE TABLE `cart_rule_coupons_usage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` int(10) unsigned NOT NULL,
  `channel_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `usage` bigint(20) unsigned NOT NULL DEFAULT 0,
  `expired_on` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_rule_coupons_usage_coupon_id_foreign` (`coupon_id`),
  KEY `cart_rule_coupons_usage_channel_id_foreign` (`channel_id`),
  KEY `cart_rule_coupons_usage_customer_id_foreign` (`customer_id`),
  CONSTRAINT `cart_rule_coupons_usage_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cart_rule_coupons_usage_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `cart_rules` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cart_rule_coupons_usage_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart_rule_coupons_usage
-- ----------------------------

-- ----------------------------
-- Table structure for cart_rule_customers
-- ----------------------------
DROP TABLE IF EXISTS `cart_rule_customers`;
CREATE TABLE `cart_rule_customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart_rule_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `usage_throttle` bigint(20) unsigned NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_rule_customers_cart_rule_id_foreign` (`cart_rule_id`),
  KEY `cart_rule_customers_customer_id_foreign` (`customer_id`),
  CONSTRAINT `cart_rule_customers_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cart_rule_customers_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart_rule_customers
-- ----------------------------

-- ----------------------------
-- Table structure for cart_rule_customer_groups
-- ----------------------------
DROP TABLE IF EXISTS `cart_rule_customer_groups`;
CREATE TABLE `cart_rule_customer_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart_rule_id` int(10) unsigned NOT NULL,
  `customer_group_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_rule_customer_groups_cart_rule_id_foreign` (`cart_rule_id`),
  KEY `cart_rule_customer_groups_customer_group_id_foreign` (`customer_group_id`),
  CONSTRAINT `cart_rule_customer_groups_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cart_rule_customer_groups_customer_group_id_foreign` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart_rule_customer_groups
-- ----------------------------

-- ----------------------------
-- Table structure for cart_rule_labels
-- ----------------------------
DROP TABLE IF EXISTS `cart_rule_labels`;
CREATE TABLE `cart_rule_labels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(10) unsigned DEFAULT NULL,
  `locale_id` int(10) unsigned DEFAULT NULL,
  `cart_rule_id` int(10) unsigned DEFAULT NULL,
  `label` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_rule_labels_channel_id_foreign` (`channel_id`),
  KEY `cart_rule_labels_locale_id_foreign` (`locale_id`),
  KEY `cart_rule_labels_cart_rule_id_foreign` (`cart_rule_id`),
  CONSTRAINT `cart_rule_labels_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cart_rule_labels_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cart_rule_labels_locale_id_foreign` FOREIGN KEY (`locale_id`) REFERENCES `locales` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart_rule_labels
-- ----------------------------

-- ----------------------------
-- Table structure for cart_shipping_rates
-- ----------------------------
DROP TABLE IF EXISTS `cart_shipping_rates`;
CREATE TABLE `cart_shipping_rates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `carrier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `carrier_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double DEFAULT 0,
  `base_price` double DEFAULT 0,
  `cart_address_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_shipping_rates_cart_address_id_foreign` (`cart_address_id`),
  CONSTRAINT `cart_shipping_rates_cart_address_id_foreign` FOREIGN KEY (`cart_address_id`) REFERENCES `cart_address` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart_shipping_rates
-- ----------------------------

-- ----------------------------
-- Table structure for catalog_rules
-- ----------------------------
DROP TABLE IF EXISTS `catalog_rules`;
CREATE TABLE `catalog_rules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `starts_from` datetime DEFAULT NULL,
  `ends_till` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `conditions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `actions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `end_other_rules` tinyint(1) NOT NULL DEFAULT 0,
  `action_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_amount` decimal(20,4) NOT NULL DEFAULT 0.0000,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of catalog_rules
-- ----------------------------

-- ----------------------------
-- Table structure for catalog_rule_channels
-- ----------------------------
DROP TABLE IF EXISTS `catalog_rule_channels`;
CREATE TABLE `catalog_rule_channels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(10) unsigned NOT NULL,
  `catalog_rule_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `catalog_rule_channels_channel_id_foreign` (`channel_id`),
  KEY `catalog_rule_channels_catalog_rule_id_foreign` (`catalog_rule_id`),
  CONSTRAINT `catalog_rule_channels_catalog_rule_id_foreign` FOREIGN KEY (`catalog_rule_id`) REFERENCES `catalog_rules` (`id`) ON DELETE CASCADE,
  CONSTRAINT `catalog_rule_channels_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of catalog_rule_channels
-- ----------------------------

-- ----------------------------
-- Table structure for catalog_rule_customer_groups
-- ----------------------------
DROP TABLE IF EXISTS `catalog_rule_customer_groups`;
CREATE TABLE `catalog_rule_customer_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_group_id` int(10) unsigned NOT NULL,
  `catalog_rule_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `catalog_rule_customer_groups_customer_group_id_foreign` (`customer_group_id`),
  KEY `catalog_rule_customer_groups_catalog_rule_id_foreign` (`catalog_rule_id`),
  CONSTRAINT `catalog_rule_customer_groups_catalog_rule_id_foreign` FOREIGN KEY (`catalog_rule_id`) REFERENCES `catalog_rules` (`id`) ON DELETE CASCADE,
  CONSTRAINT `catalog_rule_customer_groups_customer_group_id_foreign` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of catalog_rule_customer_groups
-- ----------------------------

-- ----------------------------
-- Table structure for catalog_rule_products
-- ----------------------------
DROP TABLE IF EXISTS `catalog_rule_products`;
CREATE TABLE `catalog_rule_products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `catalog_rule_id` int(10) unsigned NOT NULL,
  `starts_from` datetime DEFAULT NULL,
  `ends_till` datetime DEFAULT NULL,
  `customer_group_id` int(10) unsigned NOT NULL,
  `channel_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `action_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_amount` decimal(20,4) NOT NULL DEFAULT 0.0000,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `catalog_rule_products_catalog_rule_id_foreign` (`catalog_rule_id`),
  KEY `catalog_rule_products_customer_group_id_foreign` (`customer_group_id`),
  KEY `catalog_rule_products_channel_id_foreign` (`channel_id`),
  KEY `catalog_rule_products_product_id_foreign` (`product_id`),
  CONSTRAINT `catalog_rule_products_catalog_rule_id_foreign` FOREIGN KEY (`catalog_rule_id`) REFERENCES `catalog_rules` (`id`) ON DELETE CASCADE,
  CONSTRAINT `catalog_rule_products_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  CONSTRAINT `catalog_rule_products_customer_group_id_foreign` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `catalog_rule_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of catalog_rule_products
-- ----------------------------

-- ----------------------------
-- Table structure for catalog_rule_products_price
-- ----------------------------
DROP TABLE IF EXISTS `catalog_rule_products_price`;
CREATE TABLE `catalog_rule_products_price` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(10) unsigned NOT NULL,
  `customer_group_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `catalog_rule_id` int(10) unsigned NOT NULL,
  `rule_price` decimal(20,4) NOT NULL DEFAULT 0.0000,
  `starts_from` datetime DEFAULT NULL,
  `ends_till` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `catalog_rule_products_price_channel_id_foreign` (`channel_id`),
  KEY `catalog_rule_products_price_customer_group_id_foreign` (`customer_group_id`),
  KEY `catalog_rule_products_price_product_id_foreign` (`product_id`),
  KEY `catalog_rule_products_price_catalog_rule_id_foreign` (`catalog_rule_id`),
  CONSTRAINT `catalog_rule_products_price_catalog_rule_id_foreign` FOREIGN KEY (`catalog_rule_id`) REFERENCES `catalog_rules` (`id`) ON DELETE CASCADE,
  CONSTRAINT `catalog_rule_products_price_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  CONSTRAINT `catalog_rule_products_price_customer_group_id_foreign` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `catalog_rule_products_price_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of catalog_rule_products_price
-- ----------------------------

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position` int(11) NOT NULL DEFAULT 0,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `_lft` int(10) unsigned NOT NULL DEFAULT 0,
  `_rgt` int(10) unsigned NOT NULL DEFAULT 0,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `display_mode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'products_and_description',
  PRIMARY KEY (`id`),
  KEY `categories__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', '1', null, '1', '1', '14', null, '2019-10-02 14:15:30', '2019-10-02 14:15:30', 'products_and_description');

-- ----------------------------
-- Table structure for category_filterable_attributes
-- ----------------------------
DROP TABLE IF EXISTS `category_filterable_attributes`;
CREATE TABLE `category_filterable_attributes` (
  `category_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  KEY `category_filterable_attributes_category_id_foreign` (`category_id`),
  KEY `category_filterable_attributes_attribute_id_foreign` (`attribute_id`),
  CONSTRAINT `category_filterable_attributes_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `category_filterable_attributes_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of category_filterable_attributes
-- ----------------------------

-- ----------------------------
-- Table structure for category_translations
-- ----------------------------
DROP TABLE IF EXISTS `category_translations`;
CREATE TABLE `category_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_translations_category_id_slug_locale_unique` (`category_id`,`slug`,`locale`),
  KEY `category_translations_locale_id_foreign` (`locale_id`),
  CONSTRAINT `category_translations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `category_translations_locale_id_foreign` FOREIGN KEY (`locale_id`) REFERENCES `locales` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of category_translations
-- ----------------------------
INSERT INTO `category_translations` VALUES ('1', 'Root', 'root', 'Root', '', '', '', '1', 'en', null);

-- ----------------------------
-- Table structure for channels
-- ----------------------------
DROP TABLE IF EXISTS `channels`;
CREATE TABLE `channels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hostname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_page_content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_locale_id` int(10) unsigned NOT NULL,
  `base_currency_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `root_category_id` int(10) unsigned DEFAULT NULL,
  `home_seo` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `channels_default_locale_id_foreign` (`default_locale_id`),
  KEY `channels_base_currency_id_foreign` (`base_currency_id`),
  KEY `channels_root_category_id_foreign` (`root_category_id`),
  CONSTRAINT `channels_base_currency_id_foreign` FOREIGN KEY (`base_currency_id`) REFERENCES `currencies` (`id`),
  CONSTRAINT `channels_default_locale_id_foreign` FOREIGN KEY (`default_locale_id`) REFERENCES `locales` (`id`),
  CONSTRAINT `channels_root_category_id_foreign` FOREIGN KEY (`root_category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of channels
-- ----------------------------
INSERT INTO `channels` VALUES ('1', 'default', 'Default', null, null, null, null, null, null, '<p>@include(\"shop::home.slider\") @include(\"shop::home.featured-products\") @include(\"shop::home.new-products\")</p><div class=\"banner-container\"><div class=\"left-banner\"><img src=\"https://s3-ap-southeast-1.amazonaws.com/cdn.uvdesk.com/website/1/201902045c581f9494b8a1.png\" /></div><div class=\"right-banner\"><img src=\"https://s3-ap-southeast-1.amazonaws.com/cdn.uvdesk.com/website/1/201902045c581fb045cf02.png\" /> <img src=\"https://s3-ap-southeast-1.amazonaws.com/cdn.uvdesk.com/website/1/201902045c581fc352d803.png\" /></div></div>', '<div class=\"list-container\"><span class=\"list-heading\">Quick Links</span><ul class=\"list-group\"><li><a href=\"@php echo route(\"shop.cms.page\", \"about-us\") @endphp\">About Us</a></li><li><a href=\"@php echo route(\"shop.cms.page\", \"return-policy\") @endphp\">Return Policy</a></li><li><a href=\"@php echo route(\"shop.cms.page\", \"refund-policy\") @endphp\">Refund Policy</a></li><li><a href=\"@php echo route(\"shop.cms.page\", \"terms-conditions\") @endphp\">Terms and conditions</a></li><li><a href=\"@php echo route(\"shop.cms.page\", \"terms-of-use\") @endphp\">Terms of Use</a></li><li><a href=\"@php echo route(\"shop.cms.page\", \"contact-us\") @endphp\">Contact Us</a></li></ul></div><div class=\"list-container\"><span class=\"list-heading\">Connect With Us</span><ul class=\"list-group\"><li><a href=\"#\"><span class=\"icon icon-facebook\"></span>Facebook </a></li><li><a href=\"#\"><span class=\"icon icon-twitter\"></span> Twitter </a></li><li><a href=\"#\"><span class=\"icon icon-instagram\"></span> Instagram </a></li><li><a href=\"#\"> <span class=\"icon icon-google-plus\"></span>Google+ </a></li><li><a href=\"#\"> <span class=\"icon icon-linkedin\"></span>LinkedIn </a></li></ul></div>', '1', '1', null, null, '1', null);

-- ----------------------------
-- Table structure for channel_currencies
-- ----------------------------
DROP TABLE IF EXISTS `channel_currencies`;
CREATE TABLE `channel_currencies` (
  `channel_id` int(10) unsigned NOT NULL,
  `currency_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`channel_id`,`currency_id`),
  KEY `channel_currencies_currency_id_foreign` (`currency_id`),
  CONSTRAINT `channel_currencies_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  CONSTRAINT `channel_currencies_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of channel_currencies
-- ----------------------------
INSERT INTO `channel_currencies` VALUES ('1', '1');

-- ----------------------------
-- Table structure for channel_inventory_sources
-- ----------------------------
DROP TABLE IF EXISTS `channel_inventory_sources`;
CREATE TABLE `channel_inventory_sources` (
  `channel_id` int(10) unsigned NOT NULL,
  `inventory_source_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `channel_inventory_sources_channel_id_inventory_source_id_unique` (`channel_id`,`inventory_source_id`),
  KEY `channel_inventory_sources_inventory_source_id_foreign` (`inventory_source_id`),
  CONSTRAINT `channel_inventory_sources_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  CONSTRAINT `channel_inventory_sources_inventory_source_id_foreign` FOREIGN KEY (`inventory_source_id`) REFERENCES `inventory_sources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of channel_inventory_sources
-- ----------------------------
INSERT INTO `channel_inventory_sources` VALUES ('1', '1');

-- ----------------------------
-- Table structure for channel_locales
-- ----------------------------
DROP TABLE IF EXISTS `channel_locales`;
CREATE TABLE `channel_locales` (
  `channel_id` int(10) unsigned NOT NULL,
  `locale_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`channel_id`,`locale_id`),
  KEY `channel_locales_locale_id_foreign` (`locale_id`),
  CONSTRAINT `channel_locales_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  CONSTRAINT `channel_locales_locale_id_foreign` FOREIGN KEY (`locale_id`) REFERENCES `locales` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of channel_locales
-- ----------------------------
INSERT INTO `channel_locales` VALUES ('1', '1');

-- ----------------------------
-- Table structure for cms_pages
-- ----------------------------
DROP TABLE IF EXISTS `cms_pages`;
CREATE TABLE `cms_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `layout` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_id` int(10) unsigned NOT NULL,
  `locale_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_pages_channel_id_foreign` (`channel_id`),
  KEY `cms_pages_locale_id_foreign` (`locale_id`),
  CONSTRAINT `cms_pages_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cms_pages_locale_id_foreign` FOREIGN KEY (`locale_id`) REFERENCES `locales` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cms_pages
-- ----------------------------
INSERT INTO `cms_pages` VALUES ('1', 'about-us', '<div class=\"static-container one-column\">\n                                   <div class=\"mb-5\">About us page content</div>\n                                   </div>', 'About Us', 'about us', '', 'aboutus', 0x7B2268746D6C223A20223C64697620636C6173733D5C227374617469632D636F6E7461696E6572206F6E652D636F6C756D6E5C223E5C725C6E3C64697620636C6173733D5C226D622D355C223E41626F7574207573207061676520636F6E74656E743C2F6469763E5C725C6E3C2F6469763E222C0A20202020202020202020202020202020202020202020202020202020226D6574615F7469746C65223A202261626F7574207573222C0A2020202020202020202020202020202020202020202020202020202022706167655F7469746C65223A202241626F7574205573222C0A20202020202020202020202020202020202020202020202020202020226D6574615F6B6579776F726473223A202261626F7574757320222C20226D6574615F6465736372697074696F6E223A2022227D, null, '1', '1', null, null);
INSERT INTO `cms_pages` VALUES ('2', 'return-policy', '<div class=\"static-container one-column\">\n                                   <div class=\"mb-5\">Return policy page content</div>\n                                   </div>', 'Return Policy', 'return policy', '', 'return, policy', 0x7B2268746D6C223A20223C64697620636C6173733D5C227374617469632D636F6E7461696E6572206F6E652D636F6C756D6E5C223E5C725C6E3C64697620636C6173733D5C226D622D355C223E52657475726E20706F6C696379207061676520636F6E74656E743C2F6469763E5C725C6E3C2F6469763E222C0A20202020202020202020202020202020202020202020202020202020226D6574615F7469746C65223A202272657475726E20706F6C696379222C0A2020202020202020202020202020202020202020202020202020202022706167655F7469746C65223A202252657475726E20506F6C696379222C0A20202020202020202020202020202020202020202020202020202020226D6574615F6B6579776F726473223A202272657475726E2C20706F6C69637920222C20226D6574615F6465736372697074696F6E223A2022227D, null, '1', '1', null, null);
INSERT INTO `cms_pages` VALUES ('3', 'refund-policy', '<div class=\"static-container one-column\">\n                                   <div class=\"mb-5\">Refund policy page content</div>\n                                   </div>', 'Refund Policy', 'Refund policy', '', 'refund, policy', 0x7B2268746D6C223A20223C64697620636C6173733D5C227374617469632D636F6E7461696E6572206F6E652D636F6C756D6E5C223E5C725C6E3C64697620636C6173733D5C226D622D355C223E526566756E6420706F6C696379207061676520636F6E74656E743C2F6469763E5C725C6E3C2F6469763E222C0A20202020202020202020202020202020202020202020202020202020226D6574615F7469746C65223A2022526566756E6420706F6C696379222C0A2020202020202020202020202020202020202020202020202020202022706167655F7469746C65223A2022526566756E6420506F6C696379222C0A20202020202020202020202020202020202020202020202020202020226D6574615F6B6579776F726473223A2022726566756E642C706F6C69637920222C20226D6574615F6465736372697074696F6E223A2022227D, null, '1', '1', null, null);
INSERT INTO `cms_pages` VALUES ('4', 'terms-conditions', '<div class=\"static-container one-column\">\n                                   <div class=\"mb-5\">Terms & conditions page content</div>\n                                   </div>', 'Terms & Conditions', 'Terms & Conditions', '', 'term, conditions', 0x7B2268746D6C223A20223C64697620636C6173733D5C227374617469632D636F6E7461696E6572206F6E652D636F6C756D6E5C223E5C725C6E3C64697620636C6173733D5C226D622D355C223E5465726D73202620636F6E646974696F6E73207061676520636F6E74656E743C2F6469763E5C725C6E3C2F6469763E222C0A20202020202020202020202020202020202020202020202020202020226D6574615F7469746C65223A20225465726D73202620436F6E646974696F6E73222C0A2020202020202020202020202020202020202020202020202020202022706167655F7469746C65223A20225465726D73202620436F6E646974696F6E73222C0A20202020202020202020202020202020202020202020202020202020226D6574615F6B6579776F726473223A20227465726D732C20636F6E646974696F6E7320222C20226D6574615F6465736372697074696F6E223A2022227D, null, '1', '1', null, null);
INSERT INTO `cms_pages` VALUES ('5', 'terms-of-use', '<div class=\"static-container one-column\">\n                                   <div class=\"mb-5\">Terms of use page content</div>\n                                   </div>', 'Terms of use', 'Terms of use', '', 'term, use', 0x7B2268746D6C223A20223C64697620636C6173733D5C227374617469632D636F6E7461696E6572206F6E652D636F6C756D6E5C223E5C725C6E3C64697620636C6173733D5C226D622D355C223E5465726D73206F6620757365207061676520636F6E74656E743C2F6469763E5C725C6E3C2F6469763E222C0A20202020202020202020202020202020202020202020202020202020226D6574615F7469746C65223A20225465726D73206F6620757365222C0A2020202020202020202020202020202020202020202020202020202022706167655F7469746C65223A20225465726D73206F6620757365222C0A20202020202020202020202020202020202020202020202020202020226D6574615F6B6579776F726473223A20227465726D732C2075736520222C20226D6574615F6465736372697074696F6E223A2022227D, null, '1', '1', null, null);
INSERT INTO `cms_pages` VALUES ('6', 'contact-us', '<div class=\"static-container one-column\">\n                                   <div class=\"mb-5\">Contact us page content</div>\n                                   </div>', 'Contact Us', 'Contact Us', '', 'contact, us', 0x7B2268746D6C223A20223C64697620636C6173733D5C227374617469632D636F6E7461696E6572206F6E652D636F6C756D6E5C223E5C725C6E3C64697620636C6173733D5C226D622D355C223E436F6E74616374207573207061676520636F6E74656E743C2F6469763E5C725C6E3C2F6469763E222C0A20202020202020202020202020202020202020202020202020202020226D6574615F7469746C65223A2022436F6E74616374205573222C0A2020202020202020202020202020202020202020202020202020202022706167655F7469746C65223A2022436F6E74616374205573222C0A20202020202020202020202020202020202020202020202020202020226D6574615F6B6579776F726473223A2022636F6E746163742C20757320222C20226D6574615F6465736372697074696F6E223A2022227D, null, '1', '1', null, null);

-- ----------------------------
-- Table structure for core_config
-- ----------------------------
DROP TABLE IF EXISTS `core_config`;
CREATE TABLE `core_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `channel_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_config_channel_id_foreign` (`channel_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of core_config
-- ----------------------------

-- ----------------------------
-- Table structure for countries
-- ----------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of countries
-- ----------------------------
INSERT INTO `countries` VALUES ('1', 'AF', 'Afghanistan');
INSERT INTO `countries` VALUES ('2', 'AX', 'Åland Islands');
INSERT INTO `countries` VALUES ('3', 'AL', 'Albania');
INSERT INTO `countries` VALUES ('4', 'DZ', 'Algeria');
INSERT INTO `countries` VALUES ('5', 'AS', 'American Samoa');
INSERT INTO `countries` VALUES ('6', 'AD', 'Andorra');
INSERT INTO `countries` VALUES ('7', 'AO', 'Angola');
INSERT INTO `countries` VALUES ('8', 'AI', 'Anguilla');
INSERT INTO `countries` VALUES ('9', 'AQ', 'Antarctica');
INSERT INTO `countries` VALUES ('10', 'AG', 'Antigua & Barbuda');
INSERT INTO `countries` VALUES ('11', 'AR', 'Argentina');
INSERT INTO `countries` VALUES ('12', 'AM', 'Armenia');
INSERT INTO `countries` VALUES ('13', 'AW', 'Aruba');
INSERT INTO `countries` VALUES ('14', 'AC', 'Ascension Island');
INSERT INTO `countries` VALUES ('15', 'AU', 'Australia');
INSERT INTO `countries` VALUES ('16', 'AT', 'Austria');
INSERT INTO `countries` VALUES ('17', 'AZ', 'Azerbaijan');
INSERT INTO `countries` VALUES ('18', 'BS', 'Bahamas');
INSERT INTO `countries` VALUES ('19', 'BH', 'Bahrain');
INSERT INTO `countries` VALUES ('20', 'BD', 'Bangladesh');
INSERT INTO `countries` VALUES ('21', 'BB', 'Barbados');
INSERT INTO `countries` VALUES ('22', 'BY', 'Belarus');
INSERT INTO `countries` VALUES ('23', 'BE', 'Belgium');
INSERT INTO `countries` VALUES ('24', 'BZ', 'Belize');
INSERT INTO `countries` VALUES ('25', 'BJ', 'Benin');
INSERT INTO `countries` VALUES ('26', 'BM', 'Bermuda');
INSERT INTO `countries` VALUES ('27', 'BT', 'Bhutan');
INSERT INTO `countries` VALUES ('28', 'BO', 'Bolivia');
INSERT INTO `countries` VALUES ('29', 'BA', 'Bosnia & Herzegovina');
INSERT INTO `countries` VALUES ('30', 'BW', 'Botswana');
INSERT INTO `countries` VALUES ('31', 'BR', 'Brazil');
INSERT INTO `countries` VALUES ('32', 'IO', 'British Indian Ocean Territory');
INSERT INTO `countries` VALUES ('33', 'VG', 'British Virgin Islands');
INSERT INTO `countries` VALUES ('34', 'BN', 'Brunei');
INSERT INTO `countries` VALUES ('35', 'BG', 'Bulgaria');
INSERT INTO `countries` VALUES ('36', 'BF', 'Burkina Faso');
INSERT INTO `countries` VALUES ('37', 'BI', 'Burundi');
INSERT INTO `countries` VALUES ('38', 'KH', 'Cambodia');
INSERT INTO `countries` VALUES ('39', 'CM', 'Cameroon');
INSERT INTO `countries` VALUES ('40', 'CA', 'Canada');
INSERT INTO `countries` VALUES ('41', 'IC', 'Canary Islands');
INSERT INTO `countries` VALUES ('42', 'CV', 'Cape Verde');
INSERT INTO `countries` VALUES ('43', 'BQ', 'Caribbean Netherlands');
INSERT INTO `countries` VALUES ('44', 'KY', 'Cayman Islands');
INSERT INTO `countries` VALUES ('45', 'CF', 'Central African Republic');
INSERT INTO `countries` VALUES ('46', 'EA', 'Ceuta & Melilla');
INSERT INTO `countries` VALUES ('47', 'TD', 'Chad');
INSERT INTO `countries` VALUES ('48', 'CL', 'Chile');
INSERT INTO `countries` VALUES ('49', 'CN', 'China');
INSERT INTO `countries` VALUES ('50', 'CX', 'Christmas Island');
INSERT INTO `countries` VALUES ('51', 'CC', 'Cocos (Keeling) Islands');
INSERT INTO `countries` VALUES ('52', 'CO', 'Colombia');
INSERT INTO `countries` VALUES ('53', 'KM', 'Comoros');
INSERT INTO `countries` VALUES ('54', 'CG', 'Congo - Brazzaville');
INSERT INTO `countries` VALUES ('55', 'CD', 'Congo - Kinshasa');
INSERT INTO `countries` VALUES ('56', 'CK', 'Cook Islands');
INSERT INTO `countries` VALUES ('57', 'CR', 'Costa Rica');
INSERT INTO `countries` VALUES ('58', 'CI', 'Côte d’Ivoire');
INSERT INTO `countries` VALUES ('59', 'HR', 'Croatia');
INSERT INTO `countries` VALUES ('60', 'CU', 'Cuba');
INSERT INTO `countries` VALUES ('61', 'CW', 'Curaçao');
INSERT INTO `countries` VALUES ('62', 'CY', 'Cyprus');
INSERT INTO `countries` VALUES ('63', 'CZ', 'Czechia');
INSERT INTO `countries` VALUES ('64', 'DK', 'Denmark');
INSERT INTO `countries` VALUES ('65', 'DG', 'Diego Garcia');
INSERT INTO `countries` VALUES ('66', 'DJ', 'Djibouti');
INSERT INTO `countries` VALUES ('67', 'DM', 'Dominica');
INSERT INTO `countries` VALUES ('68', 'DO', 'Dominican Republic');
INSERT INTO `countries` VALUES ('69', 'EC', 'Ecuador');
INSERT INTO `countries` VALUES ('70', 'EG', 'Egypt');
INSERT INTO `countries` VALUES ('71', 'SV', 'El Salvador');
INSERT INTO `countries` VALUES ('72', 'GQ', 'Equatorial Guinea');
INSERT INTO `countries` VALUES ('73', 'ER', 'Eritrea');
INSERT INTO `countries` VALUES ('74', 'EE', 'Estonia');
INSERT INTO `countries` VALUES ('75', 'ET', 'Ethiopia');
INSERT INTO `countries` VALUES ('76', 'EZ', 'Eurozone');
INSERT INTO `countries` VALUES ('77', 'FK', 'Falkland Islands');
INSERT INTO `countries` VALUES ('78', 'FO', 'Faroe Islands');
INSERT INTO `countries` VALUES ('79', 'FJ', 'Fiji');
INSERT INTO `countries` VALUES ('80', 'FI', 'Finland');
INSERT INTO `countries` VALUES ('81', 'FR', 'France');
INSERT INTO `countries` VALUES ('82', 'GF', 'French Guiana');
INSERT INTO `countries` VALUES ('83', 'PF', 'French Polynesia');
INSERT INTO `countries` VALUES ('84', 'TF', 'French Southern Territories');
INSERT INTO `countries` VALUES ('85', 'GA', 'Gabon');
INSERT INTO `countries` VALUES ('86', 'GM', 'Gambia');
INSERT INTO `countries` VALUES ('87', 'GE', 'Georgia');
INSERT INTO `countries` VALUES ('88', 'DE', 'Germany');
INSERT INTO `countries` VALUES ('89', 'GH', 'Ghana');
INSERT INTO `countries` VALUES ('90', 'GI', 'Gibraltar');
INSERT INTO `countries` VALUES ('91', 'GR', 'Greece');
INSERT INTO `countries` VALUES ('92', 'GL', 'Greenland');
INSERT INTO `countries` VALUES ('93', 'GD', 'Grenada');
INSERT INTO `countries` VALUES ('94', 'GP', 'Guadeloupe');
INSERT INTO `countries` VALUES ('95', 'GU', 'Guam');
INSERT INTO `countries` VALUES ('96', 'GT', 'Guatemala');
INSERT INTO `countries` VALUES ('97', 'GG', 'Guernsey');
INSERT INTO `countries` VALUES ('98', 'GN', 'Guinea');
INSERT INTO `countries` VALUES ('99', 'GW', 'Guinea-Bissau');
INSERT INTO `countries` VALUES ('100', 'GY', 'Guyana');
INSERT INTO `countries` VALUES ('101', 'HT', 'Haiti');
INSERT INTO `countries` VALUES ('102', 'HN', 'Honduras');
INSERT INTO `countries` VALUES ('103', 'HK', 'Hong Kong SAR China');
INSERT INTO `countries` VALUES ('104', 'HU', 'Hungary');
INSERT INTO `countries` VALUES ('105', 'IS', 'Iceland');
INSERT INTO `countries` VALUES ('106', 'IN', 'India');
INSERT INTO `countries` VALUES ('107', 'ID', 'Indonesia');
INSERT INTO `countries` VALUES ('108', 'IR', 'Iran');
INSERT INTO `countries` VALUES ('109', 'IQ', 'Iraq');
INSERT INTO `countries` VALUES ('110', 'IE', 'Ireland');
INSERT INTO `countries` VALUES ('111', 'IM', 'Isle of Man');
INSERT INTO `countries` VALUES ('112', 'IL', 'Israel');
INSERT INTO `countries` VALUES ('113', 'IT', 'Italy');
INSERT INTO `countries` VALUES ('114', 'JM', 'Jamaica');
INSERT INTO `countries` VALUES ('115', 'JP', 'Japan');
INSERT INTO `countries` VALUES ('116', 'JE', 'Jersey');
INSERT INTO `countries` VALUES ('117', 'JO', 'Jordan');
INSERT INTO `countries` VALUES ('118', 'KZ', 'Kazakhstan');
INSERT INTO `countries` VALUES ('119', 'KE', 'Kenya');
INSERT INTO `countries` VALUES ('120', 'KI', 'Kiribati');
INSERT INTO `countries` VALUES ('121', 'XK', 'Kosovo');
INSERT INTO `countries` VALUES ('122', 'KW', 'Kuwait');
INSERT INTO `countries` VALUES ('123', 'KG', 'Kyrgyzstan');
INSERT INTO `countries` VALUES ('124', 'LA', 'Laos');
INSERT INTO `countries` VALUES ('125', 'LV', 'Latvia');
INSERT INTO `countries` VALUES ('126', 'LB', 'Lebanon');
INSERT INTO `countries` VALUES ('127', 'LS', 'Lesotho');
INSERT INTO `countries` VALUES ('128', 'LR', 'Liberia');
INSERT INTO `countries` VALUES ('129', 'LY', 'Libya');
INSERT INTO `countries` VALUES ('130', 'LI', 'Liechtenstein');
INSERT INTO `countries` VALUES ('131', 'LT', 'Lithuania');
INSERT INTO `countries` VALUES ('132', 'LU', 'Luxembourg');
INSERT INTO `countries` VALUES ('133', 'MO', 'Macau SAR China');
INSERT INTO `countries` VALUES ('134', 'MK', 'Macedonia');
INSERT INTO `countries` VALUES ('135', 'MG', 'Madagascar');
INSERT INTO `countries` VALUES ('136', 'MW', 'Malawi');
INSERT INTO `countries` VALUES ('137', 'MY', 'Malaysia');
INSERT INTO `countries` VALUES ('138', 'MV', 'Maldives');
INSERT INTO `countries` VALUES ('139', 'ML', 'Mali');
INSERT INTO `countries` VALUES ('140', 'MT', 'Malta');
INSERT INTO `countries` VALUES ('141', 'MH', 'Marshall Islands');
INSERT INTO `countries` VALUES ('142', 'MQ', 'Martinique');
INSERT INTO `countries` VALUES ('143', 'MR', 'Mauritania');
INSERT INTO `countries` VALUES ('144', 'MU', 'Mauritius');
INSERT INTO `countries` VALUES ('145', 'YT', 'Mayotte');
INSERT INTO `countries` VALUES ('146', 'MX', 'Mexico');
INSERT INTO `countries` VALUES ('147', 'FM', 'Micronesia');
INSERT INTO `countries` VALUES ('148', 'MD', 'Moldova');
INSERT INTO `countries` VALUES ('149', 'MC', 'Monaco');
INSERT INTO `countries` VALUES ('150', 'MN', 'Mongolia');
INSERT INTO `countries` VALUES ('151', 'ME', 'Montenegro');
INSERT INTO `countries` VALUES ('152', 'MS', 'Montserrat');
INSERT INTO `countries` VALUES ('153', 'MA', 'Morocco');
INSERT INTO `countries` VALUES ('154', 'MZ', 'Mozambique');
INSERT INTO `countries` VALUES ('155', 'MM', 'Myanmar (Burma)');
INSERT INTO `countries` VALUES ('156', 'NA', 'Namibia');
INSERT INTO `countries` VALUES ('157', 'NR', 'Nauru');
INSERT INTO `countries` VALUES ('158', 'NP', 'Nepal');
INSERT INTO `countries` VALUES ('159', 'NL', 'Netherlands');
INSERT INTO `countries` VALUES ('160', 'NC', 'New Caledonia');
INSERT INTO `countries` VALUES ('161', 'NZ', 'New Zealand');
INSERT INTO `countries` VALUES ('162', 'NI', 'Nicaragua');
INSERT INTO `countries` VALUES ('163', 'NE', 'Niger');
INSERT INTO `countries` VALUES ('164', 'NG', 'Nigeria');
INSERT INTO `countries` VALUES ('165', 'NU', 'Niue');
INSERT INTO `countries` VALUES ('166', 'NF', 'Norfolk Island');
INSERT INTO `countries` VALUES ('167', 'KP', 'North Korea');
INSERT INTO `countries` VALUES ('168', 'MP', 'Northern Mariana Islands');
INSERT INTO `countries` VALUES ('169', 'NO', 'Norway');
INSERT INTO `countries` VALUES ('170', 'OM', 'Oman');
INSERT INTO `countries` VALUES ('171', 'PK', 'Pakistan');
INSERT INTO `countries` VALUES ('172', 'PW', 'Palau');
INSERT INTO `countries` VALUES ('173', 'PS', 'Palestinian Territories');
INSERT INTO `countries` VALUES ('174', 'PA', 'Panama');
INSERT INTO `countries` VALUES ('175', 'PG', 'Papua New Guinea');
INSERT INTO `countries` VALUES ('176', 'PY', 'Paraguay');
INSERT INTO `countries` VALUES ('177', 'PE', 'Peru');
INSERT INTO `countries` VALUES ('178', 'PH', 'Philippines');
INSERT INTO `countries` VALUES ('179', 'PN', 'Pitcairn Islands');
INSERT INTO `countries` VALUES ('180', 'PL', 'Poland');
INSERT INTO `countries` VALUES ('181', 'PT', 'Portugal');
INSERT INTO `countries` VALUES ('182', 'PR', 'Puerto Rico');
INSERT INTO `countries` VALUES ('183', 'QA', 'Qatar');
INSERT INTO `countries` VALUES ('184', 'RE', 'Réunion');
INSERT INTO `countries` VALUES ('185', 'RO', 'Romania');
INSERT INTO `countries` VALUES ('186', 'RU', 'Russia');
INSERT INTO `countries` VALUES ('187', 'RW', 'Rwanda');
INSERT INTO `countries` VALUES ('188', 'WS', 'Samoa');
INSERT INTO `countries` VALUES ('189', 'SM', 'San Marino');
INSERT INTO `countries` VALUES ('190', 'ST', 'São Tomé & Príncipe');
INSERT INTO `countries` VALUES ('191', 'SA', 'Saudi Arabia');
INSERT INTO `countries` VALUES ('192', 'SN', 'Senegal');
INSERT INTO `countries` VALUES ('193', 'RS', 'Serbia');
INSERT INTO `countries` VALUES ('194', 'SC', 'Seychelles');
INSERT INTO `countries` VALUES ('195', 'SL', 'Sierra Leone');
INSERT INTO `countries` VALUES ('196', 'SG', 'Singapore');
INSERT INTO `countries` VALUES ('197', 'SX', 'Sint Maarten');
INSERT INTO `countries` VALUES ('198', 'SK', 'Slovakia');
INSERT INTO `countries` VALUES ('199', 'SI', 'Slovenia');
INSERT INTO `countries` VALUES ('200', 'SB', 'Solomon Islands');
INSERT INTO `countries` VALUES ('201', 'SO', 'Somalia');
INSERT INTO `countries` VALUES ('202', 'ZA', 'South Africa');
INSERT INTO `countries` VALUES ('203', 'GS', 'South Georgia & South Sandwich Islands');
INSERT INTO `countries` VALUES ('204', 'KR', 'South Korea');
INSERT INTO `countries` VALUES ('205', 'SS', 'South Sudan');
INSERT INTO `countries` VALUES ('206', 'ES', 'Spain');
INSERT INTO `countries` VALUES ('207', 'LK', 'Sri Lanka');
INSERT INTO `countries` VALUES ('208', 'BL', 'St. Barthélemy');
INSERT INTO `countries` VALUES ('209', 'SH', 'St. Helena');
INSERT INTO `countries` VALUES ('210', 'KN', 'St. Kitts & Nevis');
INSERT INTO `countries` VALUES ('211', 'LC', 'St. Lucia');
INSERT INTO `countries` VALUES ('212', 'MF', 'St. Martin');
INSERT INTO `countries` VALUES ('213', 'PM', 'St. Pierre & Miquelon');
INSERT INTO `countries` VALUES ('214', 'VC', 'St. Vincent & Grenadines');
INSERT INTO `countries` VALUES ('215', 'SD', 'Sudan');
INSERT INTO `countries` VALUES ('216', 'SR', 'Suriname');
INSERT INTO `countries` VALUES ('217', 'SJ', 'Svalbard & Jan Mayen');
INSERT INTO `countries` VALUES ('218', 'SZ', 'Swaziland');
INSERT INTO `countries` VALUES ('219', 'SE', 'Sweden');
INSERT INTO `countries` VALUES ('220', 'CH', 'Switzerland');
INSERT INTO `countries` VALUES ('221', 'SY', 'Syria');
INSERT INTO `countries` VALUES ('222', 'TW', 'Taiwan');
INSERT INTO `countries` VALUES ('223', 'TJ', 'Tajikistan');
INSERT INTO `countries` VALUES ('224', 'TZ', 'Tanzania');
INSERT INTO `countries` VALUES ('225', 'TH', 'Thailand');
INSERT INTO `countries` VALUES ('226', 'TL', 'Timor-Leste');
INSERT INTO `countries` VALUES ('227', 'TG', 'Togo');
INSERT INTO `countries` VALUES ('228', 'TK', 'Tokelau');
INSERT INTO `countries` VALUES ('229', 'TO', 'Tonga');
INSERT INTO `countries` VALUES ('230', 'TT', 'Trinidad & Tobago');
INSERT INTO `countries` VALUES ('231', 'TA', 'Tristan da Cunha');
INSERT INTO `countries` VALUES ('232', 'TN', 'Tunisia');
INSERT INTO `countries` VALUES ('233', 'TR', 'Turkey');
INSERT INTO `countries` VALUES ('234', 'TM', 'Turkmenistan');
INSERT INTO `countries` VALUES ('235', 'TC', 'Turks & Caicos Islands');
INSERT INTO `countries` VALUES ('236', 'TV', 'Tuvalu');
INSERT INTO `countries` VALUES ('237', 'UM', 'U.S. Outlying Islands');
INSERT INTO `countries` VALUES ('238', 'VI', 'U.S. Virgin Islands');
INSERT INTO `countries` VALUES ('239', 'UG', 'Uganda');
INSERT INTO `countries` VALUES ('240', 'UA', 'Ukraine');
INSERT INTO `countries` VALUES ('241', 'AE', 'United Arab Emirates');
INSERT INTO `countries` VALUES ('242', 'GB', 'United Kingdom');
INSERT INTO `countries` VALUES ('243', 'UN', 'United Nations');
INSERT INTO `countries` VALUES ('244', 'US', 'United States');
INSERT INTO `countries` VALUES ('245', 'UY', 'Uruguay');
INSERT INTO `countries` VALUES ('246', 'UZ', 'Uzbekistan');
INSERT INTO `countries` VALUES ('247', 'VU', 'Vanuatu');
INSERT INTO `countries` VALUES ('248', 'VA', 'Vatican City');
INSERT INTO `countries` VALUES ('249', 'VE', 'Venezuela');
INSERT INTO `countries` VALUES ('250', 'VN', 'Vietnam');
INSERT INTO `countries` VALUES ('251', 'WF', 'Wallis & Futuna');
INSERT INTO `countries` VALUES ('252', 'EH', 'Western Sahara');
INSERT INTO `countries` VALUES ('253', 'YE', 'Yemen');
INSERT INTO `countries` VALUES ('254', 'ZM', 'Zambia');
INSERT INTO `countries` VALUES ('255', 'ZW', 'Zimbabwe');

-- ----------------------------
-- Table structure for country_states
-- ----------------------------
DROP TABLE IF EXISTS `country_states`;
CREATE TABLE `country_states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_states_country_id_foreign` (`country_id`),
  CONSTRAINT `country_states_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=569 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of country_states
-- ----------------------------
INSERT INTO `country_states` VALUES ('1', 'US', 'AL', 'Alabama', '244');
INSERT INTO `country_states` VALUES ('2', 'US', 'AK', 'Alaska', '244');
INSERT INTO `country_states` VALUES ('3', 'US', 'AS', 'American Samoa', '244');
INSERT INTO `country_states` VALUES ('4', 'US', 'AZ', 'Arizona', '244');
INSERT INTO `country_states` VALUES ('5', 'US', 'AR', 'Arkansas', '244');
INSERT INTO `country_states` VALUES ('6', 'US', 'AE', 'Armed Forces Africa', '244');
INSERT INTO `country_states` VALUES ('7', 'US', 'AA', 'Armed Forces Americas', '244');
INSERT INTO `country_states` VALUES ('8', 'US', 'AE', 'Armed Forces Canada', '244');
INSERT INTO `country_states` VALUES ('9', 'US', 'AE', 'Armed Forces Europe', '244');
INSERT INTO `country_states` VALUES ('10', 'US', 'AE', 'Armed Forces Middle East', '244');
INSERT INTO `country_states` VALUES ('11', 'US', 'AP', 'Armed Forces Pacific', '244');
INSERT INTO `country_states` VALUES ('12', 'US', 'CA', 'California', '244');
INSERT INTO `country_states` VALUES ('13', 'US', 'CO', 'Colorado', '244');
INSERT INTO `country_states` VALUES ('14', 'US', 'CT', 'Connecticut', '244');
INSERT INTO `country_states` VALUES ('15', 'US', 'DE', 'Delaware', '244');
INSERT INTO `country_states` VALUES ('16', 'US', 'DC', 'District of Columbia', '244');
INSERT INTO `country_states` VALUES ('17', 'US', 'FM', 'Federated States Of Micronesia', '244');
INSERT INTO `country_states` VALUES ('18', 'US', 'FL', 'Florida', '244');
INSERT INTO `country_states` VALUES ('19', 'US', 'GA', 'Georgia', '244');
INSERT INTO `country_states` VALUES ('20', 'US', 'GU', 'Guam', '244');
INSERT INTO `country_states` VALUES ('21', 'US', 'HI', 'Hawaii', '244');
INSERT INTO `country_states` VALUES ('22', 'US', 'ID', 'Idaho', '244');
INSERT INTO `country_states` VALUES ('23', 'US', 'IL', 'Illinois', '244');
INSERT INTO `country_states` VALUES ('24', 'US', 'IN', 'Indiana', '244');
INSERT INTO `country_states` VALUES ('25', 'US', 'IA', 'Iowa', '244');
INSERT INTO `country_states` VALUES ('26', 'US', 'KS', 'Kansas', '244');
INSERT INTO `country_states` VALUES ('27', 'US', 'KY', 'Kentucky', '244');
INSERT INTO `country_states` VALUES ('28', 'US', 'LA', 'Louisiana', '244');
INSERT INTO `country_states` VALUES ('29', 'US', 'ME', 'Maine', '244');
INSERT INTO `country_states` VALUES ('30', 'US', 'MH', 'Marshall Islands', '244');
INSERT INTO `country_states` VALUES ('31', 'US', 'MD', 'Maryland', '244');
INSERT INTO `country_states` VALUES ('32', 'US', 'MA', 'Massachusetts', '244');
INSERT INTO `country_states` VALUES ('33', 'US', 'MI', 'Michigan', '244');
INSERT INTO `country_states` VALUES ('34', 'US', 'MN', 'Minnesota', '244');
INSERT INTO `country_states` VALUES ('35', 'US', 'MS', 'Mississippi', '244');
INSERT INTO `country_states` VALUES ('36', 'US', 'MO', 'Missouri', '244');
INSERT INTO `country_states` VALUES ('37', 'US', 'MT', 'Montana', '244');
INSERT INTO `country_states` VALUES ('38', 'US', 'NE', 'Nebraska', '244');
INSERT INTO `country_states` VALUES ('39', 'US', 'NV', 'Nevada', '244');
INSERT INTO `country_states` VALUES ('40', 'US', 'NH', 'New Hampshire', '244');
INSERT INTO `country_states` VALUES ('41', 'US', 'NJ', 'New Jersey', '244');
INSERT INTO `country_states` VALUES ('42', 'US', 'NM', 'New Mexico', '244');
INSERT INTO `country_states` VALUES ('43', 'US', 'NY', 'New York', '244');
INSERT INTO `country_states` VALUES ('44', 'US', 'NC', 'North Carolina', '244');
INSERT INTO `country_states` VALUES ('45', 'US', 'ND', 'North Dakota', '244');
INSERT INTO `country_states` VALUES ('46', 'US', 'MP', 'Northern Mariana Islands', '244');
INSERT INTO `country_states` VALUES ('47', 'US', 'OH', 'Ohio', '244');
INSERT INTO `country_states` VALUES ('48', 'US', 'OK', 'Oklahoma', '244');
INSERT INTO `country_states` VALUES ('49', 'US', 'OR', 'Oregon', '244');
INSERT INTO `country_states` VALUES ('50', 'US', 'PW', 'Palau', '244');
INSERT INTO `country_states` VALUES ('51', 'US', 'PA', 'Pennsylvania', '244');
INSERT INTO `country_states` VALUES ('52', 'US', 'PR', 'Puerto Rico', '244');
INSERT INTO `country_states` VALUES ('53', 'US', 'RI', 'Rhode Island', '244');
INSERT INTO `country_states` VALUES ('54', 'US', 'SC', 'South Carolina', '244');
INSERT INTO `country_states` VALUES ('55', 'US', 'SD', 'South Dakota', '244');
INSERT INTO `country_states` VALUES ('56', 'US', 'TN', 'Tennessee', '244');
INSERT INTO `country_states` VALUES ('57', 'US', 'TX', 'Texas', '244');
INSERT INTO `country_states` VALUES ('58', 'US', 'UT', 'Utah', '244');
INSERT INTO `country_states` VALUES ('59', 'US', 'VT', 'Vermont', '244');
INSERT INTO `country_states` VALUES ('60', 'US', 'VI', 'Virgin Islands', '244');
INSERT INTO `country_states` VALUES ('61', 'US', 'VA', 'Virginia', '244');
INSERT INTO `country_states` VALUES ('62', 'US', 'WA', 'Washington', '244');
INSERT INTO `country_states` VALUES ('63', 'US', 'WV', 'West Virginia', '244');
INSERT INTO `country_states` VALUES ('64', 'US', 'WI', 'Wisconsin', '244');
INSERT INTO `country_states` VALUES ('65', 'US', 'WY', 'Wyoming', '244');
INSERT INTO `country_states` VALUES ('66', 'CA', 'AB', 'Alberta', '40');
INSERT INTO `country_states` VALUES ('67', 'CA', 'BC', 'British Columbia', '40');
INSERT INTO `country_states` VALUES ('68', 'CA', 'MB', 'Manitoba', '40');
INSERT INTO `country_states` VALUES ('69', 'CA', 'NL', 'Newfoundland and Labrador', '40');
INSERT INTO `country_states` VALUES ('70', 'CA', 'NB', 'New Brunswick', '40');
INSERT INTO `country_states` VALUES ('71', 'CA', 'NS', 'Nova Scotia', '40');
INSERT INTO `country_states` VALUES ('72', 'CA', 'NT', 'Northwest Territories', '40');
INSERT INTO `country_states` VALUES ('73', 'CA', 'NU', 'Nunavut', '40');
INSERT INTO `country_states` VALUES ('74', 'CA', 'ON', 'Ontario', '40');
INSERT INTO `country_states` VALUES ('75', 'CA', 'PE', 'Prince Edward Island', '40');
INSERT INTO `country_states` VALUES ('76', 'CA', 'QC', 'Quebec', '40');
INSERT INTO `country_states` VALUES ('77', 'CA', 'SK', 'Saskatchewan', '40');
INSERT INTO `country_states` VALUES ('78', 'CA', 'YT', 'Yukon Territory', '40');
INSERT INTO `country_states` VALUES ('79', 'DE', 'NDS', 'Niedersachsen', '88');
INSERT INTO `country_states` VALUES ('80', 'DE', 'BAW', 'Baden-Württemberg', '88');
INSERT INTO `country_states` VALUES ('81', 'DE', 'BAY', 'Bayern', '88');
INSERT INTO `country_states` VALUES ('82', 'DE', 'BER', 'Berlin', '88');
INSERT INTO `country_states` VALUES ('83', 'DE', 'BRG', 'Brandenburg', '88');
INSERT INTO `country_states` VALUES ('84', 'DE', 'BRE', 'Bremen', '88');
INSERT INTO `country_states` VALUES ('85', 'DE', 'HAM', 'Hamburg', '88');
INSERT INTO `country_states` VALUES ('86', 'DE', 'HES', 'Hessen', '88');
INSERT INTO `country_states` VALUES ('87', 'DE', 'MEC', 'Mecklenburg-Vorpommern', '88');
INSERT INTO `country_states` VALUES ('88', 'DE', 'NRW', 'Nordrhein-Westfalen', '88');
INSERT INTO `country_states` VALUES ('89', 'DE', 'RHE', 'Rheinland-Pfalz', '88');
INSERT INTO `country_states` VALUES ('90', 'DE', 'SAR', 'Saarland', '88');
INSERT INTO `country_states` VALUES ('91', 'DE', 'SAS', 'Sachsen', '88');
INSERT INTO `country_states` VALUES ('92', 'DE', 'SAC', 'Sachsen-Anhalt', '88');
INSERT INTO `country_states` VALUES ('93', 'DE', 'SCN', 'Schleswig-Holstein', '88');
INSERT INTO `country_states` VALUES ('94', 'DE', 'THE', 'Thüringen', '88');
INSERT INTO `country_states` VALUES ('95', 'AT', 'WI', 'Wien', '16');
INSERT INTO `country_states` VALUES ('96', 'AT', 'NO', 'Niederösterreich', '16');
INSERT INTO `country_states` VALUES ('97', 'AT', 'OO', 'Oberösterreich', '16');
INSERT INTO `country_states` VALUES ('98', 'AT', 'SB', 'Salzburg', '16');
INSERT INTO `country_states` VALUES ('99', 'AT', 'KN', 'Kärnten', '16');
INSERT INTO `country_states` VALUES ('100', 'AT', 'ST', 'Steiermark', '16');
INSERT INTO `country_states` VALUES ('101', 'AT', 'TI', 'Tirol', '16');
INSERT INTO `country_states` VALUES ('102', 'AT', 'BL', 'Burgenland', '16');
INSERT INTO `country_states` VALUES ('103', 'AT', 'VB', 'Vorarlberg', '16');
INSERT INTO `country_states` VALUES ('104', 'CH', 'AG', 'Aargau', '220');
INSERT INTO `country_states` VALUES ('105', 'CH', 'AI', 'Appenzell Innerrhoden', '220');
INSERT INTO `country_states` VALUES ('106', 'CH', 'AR', 'Appenzell Ausserrhoden', '220');
INSERT INTO `country_states` VALUES ('107', 'CH', 'BE', 'Bern', '220');
INSERT INTO `country_states` VALUES ('108', 'CH', 'BL', 'Basel-Landschaft', '220');
INSERT INTO `country_states` VALUES ('109', 'CH', 'BS', 'Basel-Stadt', '220');
INSERT INTO `country_states` VALUES ('110', 'CH', 'FR', 'Freiburg', '220');
INSERT INTO `country_states` VALUES ('111', 'CH', 'GE', 'Genf', '220');
INSERT INTO `country_states` VALUES ('112', 'CH', 'GL', 'Glarus', '220');
INSERT INTO `country_states` VALUES ('113', 'CH', 'GR', 'Graubünden', '220');
INSERT INTO `country_states` VALUES ('114', 'CH', 'JU', 'Jura', '220');
INSERT INTO `country_states` VALUES ('115', 'CH', 'LU', 'Luzern', '220');
INSERT INTO `country_states` VALUES ('116', 'CH', 'NE', 'Neuenburg', '220');
INSERT INTO `country_states` VALUES ('117', 'CH', 'NW', 'Nidwalden', '220');
INSERT INTO `country_states` VALUES ('118', 'CH', 'OW', 'Obwalden', '220');
INSERT INTO `country_states` VALUES ('119', 'CH', 'SG', 'St. Gallen', '220');
INSERT INTO `country_states` VALUES ('120', 'CH', 'SH', 'Schaffhausen', '220');
INSERT INTO `country_states` VALUES ('121', 'CH', 'SO', 'Solothurn', '220');
INSERT INTO `country_states` VALUES ('122', 'CH', 'SZ', 'Schwyz', '220');
INSERT INTO `country_states` VALUES ('123', 'CH', 'TG', 'Thurgau', '220');
INSERT INTO `country_states` VALUES ('124', 'CH', 'TI', 'Tessin', '220');
INSERT INTO `country_states` VALUES ('125', 'CH', 'UR', 'Uri', '220');
INSERT INTO `country_states` VALUES ('126', 'CH', 'VD', 'Waadt', '220');
INSERT INTO `country_states` VALUES ('127', 'CH', 'VS', 'Wallis', '220');
INSERT INTO `country_states` VALUES ('128', 'CH', 'ZG', 'Zug', '220');
INSERT INTO `country_states` VALUES ('129', 'CH', 'ZH', 'Zürich', '220');
INSERT INTO `country_states` VALUES ('130', 'ES', 'A Coruсa', 'A Coruña', '206');
INSERT INTO `country_states` VALUES ('131', 'ES', 'Alava', 'Alava', '206');
INSERT INTO `country_states` VALUES ('132', 'ES', 'Albacete', 'Albacete', '206');
INSERT INTO `country_states` VALUES ('133', 'ES', 'Alicante', 'Alicante', '206');
INSERT INTO `country_states` VALUES ('134', 'ES', 'Almeria', 'Almeria', '206');
INSERT INTO `country_states` VALUES ('135', 'ES', 'Asturias', 'Asturias', '206');
INSERT INTO `country_states` VALUES ('136', 'ES', 'Avila', 'Avila', '206');
INSERT INTO `country_states` VALUES ('137', 'ES', 'Badajoz', 'Badajoz', '206');
INSERT INTO `country_states` VALUES ('138', 'ES', 'Baleares', 'Baleares', '206');
INSERT INTO `country_states` VALUES ('139', 'ES', 'Barcelona', 'Barcelona', '206');
INSERT INTO `country_states` VALUES ('140', 'ES', 'Burgos', 'Burgos', '206');
INSERT INTO `country_states` VALUES ('141', 'ES', 'Caceres', 'Caceres', '206');
INSERT INTO `country_states` VALUES ('142', 'ES', 'Cadiz', 'Cadiz', '206');
INSERT INTO `country_states` VALUES ('143', 'ES', 'Cantabria', 'Cantabria', '206');
INSERT INTO `country_states` VALUES ('144', 'ES', 'Castellon', 'Castellon', '206');
INSERT INTO `country_states` VALUES ('145', 'ES', 'Ceuta', 'Ceuta', '206');
INSERT INTO `country_states` VALUES ('146', 'ES', 'Ciudad Real', 'Ciudad Real', '206');
INSERT INTO `country_states` VALUES ('147', 'ES', 'Cordoba', 'Cordoba', '206');
INSERT INTO `country_states` VALUES ('148', 'ES', 'Cuenca', 'Cuenca', '206');
INSERT INTO `country_states` VALUES ('149', 'ES', 'Girona', 'Girona', '206');
INSERT INTO `country_states` VALUES ('150', 'ES', 'Granada', 'Granada', '206');
INSERT INTO `country_states` VALUES ('151', 'ES', 'Guadalajara', 'Guadalajara', '206');
INSERT INTO `country_states` VALUES ('152', 'ES', 'Guipuzcoa', 'Guipuzcoa', '206');
INSERT INTO `country_states` VALUES ('153', 'ES', 'Huelva', 'Huelva', '206');
INSERT INTO `country_states` VALUES ('154', 'ES', 'Huesca', 'Huesca', '206');
INSERT INTO `country_states` VALUES ('155', 'ES', 'Jaen', 'Jaen', '206');
INSERT INTO `country_states` VALUES ('156', 'ES', 'La Rioja', 'La Rioja', '206');
INSERT INTO `country_states` VALUES ('157', 'ES', 'Las Palmas', 'Las Palmas', '206');
INSERT INTO `country_states` VALUES ('158', 'ES', 'Leon', 'Leon', '206');
INSERT INTO `country_states` VALUES ('159', 'ES', 'Lleida', 'Lleida', '206');
INSERT INTO `country_states` VALUES ('160', 'ES', 'Lugo', 'Lugo', '206');
INSERT INTO `country_states` VALUES ('161', 'ES', 'Madrid', 'Madrid', '206');
INSERT INTO `country_states` VALUES ('162', 'ES', 'Malaga', 'Malaga', '206');
INSERT INTO `country_states` VALUES ('163', 'ES', 'Melilla', 'Melilla', '206');
INSERT INTO `country_states` VALUES ('164', 'ES', 'Murcia', 'Murcia', '206');
INSERT INTO `country_states` VALUES ('165', 'ES', 'Navarra', 'Navarra', '206');
INSERT INTO `country_states` VALUES ('166', 'ES', 'Ourense', 'Ourense', '206');
INSERT INTO `country_states` VALUES ('167', 'ES', 'Palencia', 'Palencia', '206');
INSERT INTO `country_states` VALUES ('168', 'ES', 'Pontevedra', 'Pontevedra', '206');
INSERT INTO `country_states` VALUES ('169', 'ES', 'Salamanca', 'Salamanca', '206');
INSERT INTO `country_states` VALUES ('170', 'ES', 'Santa Cruz de Tenerife', 'Santa Cruz de Tenerife', '206');
INSERT INTO `country_states` VALUES ('171', 'ES', 'Segovia', 'Segovia', '206');
INSERT INTO `country_states` VALUES ('172', 'ES', 'Sevilla', 'Sevilla', '206');
INSERT INTO `country_states` VALUES ('173', 'ES', 'Soria', 'Soria', '206');
INSERT INTO `country_states` VALUES ('174', 'ES', 'Tarragona', 'Tarragona', '206');
INSERT INTO `country_states` VALUES ('175', 'ES', 'Teruel', 'Teruel', '206');
INSERT INTO `country_states` VALUES ('176', 'ES', 'Toledo', 'Toledo', '206');
INSERT INTO `country_states` VALUES ('177', 'ES', 'Valencia', 'Valencia', '206');
INSERT INTO `country_states` VALUES ('178', 'ES', 'Valladolid', 'Valladolid', '206');
INSERT INTO `country_states` VALUES ('179', 'ES', 'Vizcaya', 'Vizcaya', '206');
INSERT INTO `country_states` VALUES ('180', 'ES', 'Zamora', 'Zamora', '206');
INSERT INTO `country_states` VALUES ('181', 'ES', 'Zaragoza', 'Zaragoza', '206');
INSERT INTO `country_states` VALUES ('182', 'FR', '1', 'Ain', '81');
INSERT INTO `country_states` VALUES ('183', 'FR', '2', 'Aisne', '81');
INSERT INTO `country_states` VALUES ('184', 'FR', '3', 'Allier', '81');
INSERT INTO `country_states` VALUES ('185', 'FR', '4', 'Alpes-de-Haute-Provence', '81');
INSERT INTO `country_states` VALUES ('186', 'FR', '5', 'Hautes-Alpes', '81');
INSERT INTO `country_states` VALUES ('187', 'FR', '6', 'Alpes-Maritimes', '81');
INSERT INTO `country_states` VALUES ('188', 'FR', '7', 'Ardèche', '81');
INSERT INTO `country_states` VALUES ('189', 'FR', '8', 'Ardennes', '81');
INSERT INTO `country_states` VALUES ('190', 'FR', '9', 'Ariège', '81');
INSERT INTO `country_states` VALUES ('191', 'FR', '10', 'Aube', '81');
INSERT INTO `country_states` VALUES ('192', 'FR', '11', 'Aude', '81');
INSERT INTO `country_states` VALUES ('193', 'FR', '12', 'Aveyron', '81');
INSERT INTO `country_states` VALUES ('194', 'FR', '13', 'Bouches-du-Rhône', '81');
INSERT INTO `country_states` VALUES ('195', 'FR', '14', 'Calvados', '81');
INSERT INTO `country_states` VALUES ('196', 'FR', '15', 'Cantal', '81');
INSERT INTO `country_states` VALUES ('197', 'FR', '16', 'Charente', '81');
INSERT INTO `country_states` VALUES ('198', 'FR', '17', 'Charente-Maritime', '81');
INSERT INTO `country_states` VALUES ('199', 'FR', '18', 'Cher', '81');
INSERT INTO `country_states` VALUES ('200', 'FR', '19', 'Corrèze', '81');
INSERT INTO `country_states` VALUES ('201', 'FR', '2A', 'Corse-du-Sud', '81');
INSERT INTO `country_states` VALUES ('202', 'FR', '2B', 'Haute-Corse', '81');
INSERT INTO `country_states` VALUES ('203', 'FR', '21', 'Côte-d\'Or', '81');
INSERT INTO `country_states` VALUES ('204', 'FR', '22', 'Côtes-d\'Armor', '81');
INSERT INTO `country_states` VALUES ('205', 'FR', '23', 'Creuse', '81');
INSERT INTO `country_states` VALUES ('206', 'FR', '24', 'Dordogne', '81');
INSERT INTO `country_states` VALUES ('207', 'FR', '25', 'Doubs', '81');
INSERT INTO `country_states` VALUES ('208', 'FR', '26', 'Drôme', '81');
INSERT INTO `country_states` VALUES ('209', 'FR', '27', 'Eure', '81');
INSERT INTO `country_states` VALUES ('210', 'FR', '28', 'Eure-et-Loir', '81');
INSERT INTO `country_states` VALUES ('211', 'FR', '29', 'Finistère', '81');
INSERT INTO `country_states` VALUES ('212', 'FR', '30', 'Gard', '81');
INSERT INTO `country_states` VALUES ('213', 'FR', '31', 'Haute-Garonne', '81');
INSERT INTO `country_states` VALUES ('214', 'FR', '32', 'Gers', '81');
INSERT INTO `country_states` VALUES ('215', 'FR', '33', 'Gironde', '81');
INSERT INTO `country_states` VALUES ('216', 'FR', '34', 'Hérault', '81');
INSERT INTO `country_states` VALUES ('217', 'FR', '35', 'Ille-et-Vilaine', '81');
INSERT INTO `country_states` VALUES ('218', 'FR', '36', 'Indre', '81');
INSERT INTO `country_states` VALUES ('219', 'FR', '37', 'Indre-et-Loire', '81');
INSERT INTO `country_states` VALUES ('220', 'FR', '38', 'Isère', '81');
INSERT INTO `country_states` VALUES ('221', 'FR', '39', 'Jura', '81');
INSERT INTO `country_states` VALUES ('222', 'FR', '40', 'Landes', '81');
INSERT INTO `country_states` VALUES ('223', 'FR', '41', 'Loir-et-Cher', '81');
INSERT INTO `country_states` VALUES ('224', 'FR', '42', 'Loire', '81');
INSERT INTO `country_states` VALUES ('225', 'FR', '43', 'Haute-Loire', '81');
INSERT INTO `country_states` VALUES ('226', 'FR', '44', 'Loire-Atlantique', '81');
INSERT INTO `country_states` VALUES ('227', 'FR', '45', 'Loiret', '81');
INSERT INTO `country_states` VALUES ('228', 'FR', '46', 'Lot', '81');
INSERT INTO `country_states` VALUES ('229', 'FR', '47', 'Lot-et-Garonne', '81');
INSERT INTO `country_states` VALUES ('230', 'FR', '48', 'Lozère', '81');
INSERT INTO `country_states` VALUES ('231', 'FR', '49', 'Maine-et-Loire', '81');
INSERT INTO `country_states` VALUES ('232', 'FR', '50', 'Manche', '81');
INSERT INTO `country_states` VALUES ('233', 'FR', '51', 'Marne', '81');
INSERT INTO `country_states` VALUES ('234', 'FR', '52', 'Haute-Marne', '81');
INSERT INTO `country_states` VALUES ('235', 'FR', '53', 'Mayenne', '81');
INSERT INTO `country_states` VALUES ('236', 'FR', '54', 'Meurthe-et-Moselle', '81');
INSERT INTO `country_states` VALUES ('237', 'FR', '55', 'Meuse', '81');
INSERT INTO `country_states` VALUES ('238', 'FR', '56', 'Morbihan', '81');
INSERT INTO `country_states` VALUES ('239', 'FR', '57', 'Moselle', '81');
INSERT INTO `country_states` VALUES ('240', 'FR', '58', 'Nièvre', '81');
INSERT INTO `country_states` VALUES ('241', 'FR', '59', 'Nord', '81');
INSERT INTO `country_states` VALUES ('242', 'FR', '60', 'Oise', '81');
INSERT INTO `country_states` VALUES ('243', 'FR', '61', 'Orne', '81');
INSERT INTO `country_states` VALUES ('244', 'FR', '62', 'Pas-de-Calais', '81');
INSERT INTO `country_states` VALUES ('245', 'FR', '63', 'Puy-de-Dôme', '81');
INSERT INTO `country_states` VALUES ('246', 'FR', '64', 'Pyrénées-Atlantiques', '81');
INSERT INTO `country_states` VALUES ('247', 'FR', '65', 'Hautes-Pyrénées', '81');
INSERT INTO `country_states` VALUES ('248', 'FR', '66', 'Pyrénées-Orientales', '81');
INSERT INTO `country_states` VALUES ('249', 'FR', '67', 'Bas-Rhin', '81');
INSERT INTO `country_states` VALUES ('250', 'FR', '68', 'Haut-Rhin', '81');
INSERT INTO `country_states` VALUES ('251', 'FR', '69', 'Rhône', '81');
INSERT INTO `country_states` VALUES ('252', 'FR', '70', 'Haute-Saône', '81');
INSERT INTO `country_states` VALUES ('253', 'FR', '71', 'Saône-et-Loire', '81');
INSERT INTO `country_states` VALUES ('254', 'FR', '72', 'Sarthe', '81');
INSERT INTO `country_states` VALUES ('255', 'FR', '73', 'Savoie', '81');
INSERT INTO `country_states` VALUES ('256', 'FR', '74', 'Haute-Savoie', '81');
INSERT INTO `country_states` VALUES ('257', 'FR', '75', 'Paris', '81');
INSERT INTO `country_states` VALUES ('258', 'FR', '76', 'Seine-Maritime', '81');
INSERT INTO `country_states` VALUES ('259', 'FR', '77', 'Seine-et-Marne', '81');
INSERT INTO `country_states` VALUES ('260', 'FR', '78', 'Yvelines', '81');
INSERT INTO `country_states` VALUES ('261', 'FR', '79', 'Deux-Sèvres', '81');
INSERT INTO `country_states` VALUES ('262', 'FR', '80', 'Somme', '81');
INSERT INTO `country_states` VALUES ('263', 'FR', '81', 'Tarn', '81');
INSERT INTO `country_states` VALUES ('264', 'FR', '82', 'Tarn-et-Garonne', '81');
INSERT INTO `country_states` VALUES ('265', 'FR', '83', 'Var', '81');
INSERT INTO `country_states` VALUES ('266', 'FR', '84', 'Vaucluse', '81');
INSERT INTO `country_states` VALUES ('267', 'FR', '85', 'Vendée', '81');
INSERT INTO `country_states` VALUES ('268', 'FR', '86', 'Vienne', '81');
INSERT INTO `country_states` VALUES ('269', 'FR', '87', 'Haute-Vienne', '81');
INSERT INTO `country_states` VALUES ('270', 'FR', '88', 'Vosges', '81');
INSERT INTO `country_states` VALUES ('271', 'FR', '89', 'Yonne', '81');
INSERT INTO `country_states` VALUES ('272', 'FR', '90', 'Territoire-de-Belfort', '81');
INSERT INTO `country_states` VALUES ('273', 'FR', '91', 'Essonne', '81');
INSERT INTO `country_states` VALUES ('274', 'FR', '92', 'Hauts-de-Seine', '81');
INSERT INTO `country_states` VALUES ('275', 'FR', '93', 'Seine-Saint-Denis', '81');
INSERT INTO `country_states` VALUES ('276', 'FR', '94', 'Val-de-Marne', '81');
INSERT INTO `country_states` VALUES ('277', 'FR', '95', 'Val-d\'Oise', '81');
INSERT INTO `country_states` VALUES ('278', 'RO', 'AB', 'Alba', '185');
INSERT INTO `country_states` VALUES ('279', 'RO', 'AR', 'Arad', '185');
INSERT INTO `country_states` VALUES ('280', 'RO', 'AG', 'Argeş', '185');
INSERT INTO `country_states` VALUES ('281', 'RO', 'BC', 'Bacău', '185');
INSERT INTO `country_states` VALUES ('282', 'RO', 'BH', 'Bihor', '185');
INSERT INTO `country_states` VALUES ('283', 'RO', 'BN', 'Bistriţa-Năsăud', '185');
INSERT INTO `country_states` VALUES ('284', 'RO', 'BT', 'Botoşani', '185');
INSERT INTO `country_states` VALUES ('285', 'RO', 'BV', 'Braşov', '185');
INSERT INTO `country_states` VALUES ('286', 'RO', 'BR', 'Brăila', '185');
INSERT INTO `country_states` VALUES ('287', 'RO', 'B', 'Bucureşti', '185');
INSERT INTO `country_states` VALUES ('288', 'RO', 'BZ', 'Buzău', '185');
INSERT INTO `country_states` VALUES ('289', 'RO', 'CS', 'Caraş-Severin', '185');
INSERT INTO `country_states` VALUES ('290', 'RO', 'CL', 'Călăraşi', '185');
INSERT INTO `country_states` VALUES ('291', 'RO', 'CJ', 'Cluj', '185');
INSERT INTO `country_states` VALUES ('292', 'RO', 'CT', 'Constanţa', '185');
INSERT INTO `country_states` VALUES ('293', 'RO', 'CV', 'Covasna', '185');
INSERT INTO `country_states` VALUES ('294', 'RO', 'DB', 'Dâmboviţa', '185');
INSERT INTO `country_states` VALUES ('295', 'RO', 'DJ', 'Dolj', '185');
INSERT INTO `country_states` VALUES ('296', 'RO', 'GL', 'Galaţi', '185');
INSERT INTO `country_states` VALUES ('297', 'RO', 'GR', 'Giurgiu', '185');
INSERT INTO `country_states` VALUES ('298', 'RO', 'GJ', 'Gorj', '185');
INSERT INTO `country_states` VALUES ('299', 'RO', 'HR', 'Harghita', '185');
INSERT INTO `country_states` VALUES ('300', 'RO', 'HD', 'Hunedoara', '185');
INSERT INTO `country_states` VALUES ('301', 'RO', 'IL', 'Ialomiţa', '185');
INSERT INTO `country_states` VALUES ('302', 'RO', 'IS', 'Iaşi', '185');
INSERT INTO `country_states` VALUES ('303', 'RO', 'IF', 'Ilfov', '185');
INSERT INTO `country_states` VALUES ('304', 'RO', 'MM', 'Maramureş', '185');
INSERT INTO `country_states` VALUES ('305', 'RO', 'MH', 'Mehedinţi', '185');
INSERT INTO `country_states` VALUES ('306', 'RO', 'MS', 'Mureş', '185');
INSERT INTO `country_states` VALUES ('307', 'RO', 'NT', 'Neamţ', '185');
INSERT INTO `country_states` VALUES ('308', 'RO', 'OT', 'Olt', '185');
INSERT INTO `country_states` VALUES ('309', 'RO', 'PH', 'Prahova', '185');
INSERT INTO `country_states` VALUES ('310', 'RO', 'SM', 'Satu-Mare', '185');
INSERT INTO `country_states` VALUES ('311', 'RO', 'SJ', 'Sălaj', '185');
INSERT INTO `country_states` VALUES ('312', 'RO', 'SB', 'Sibiu', '185');
INSERT INTO `country_states` VALUES ('313', 'RO', 'SV', 'Suceava', '185');
INSERT INTO `country_states` VALUES ('314', 'RO', 'TR', 'Teleorman', '185');
INSERT INTO `country_states` VALUES ('315', 'RO', 'TM', 'Timiş', '185');
INSERT INTO `country_states` VALUES ('316', 'RO', 'TL', 'Tulcea', '185');
INSERT INTO `country_states` VALUES ('317', 'RO', 'VS', 'Vaslui', '185');
INSERT INTO `country_states` VALUES ('318', 'RO', 'VL', 'Vâlcea', '185');
INSERT INTO `country_states` VALUES ('319', 'RO', 'VN', 'Vrancea', '185');
INSERT INTO `country_states` VALUES ('320', 'FI', 'Lappi', 'Lappi', '80');
INSERT INTO `country_states` VALUES ('321', 'FI', 'Pohjois-Pohjanmaa', 'Pohjois-Pohjanmaa', '80');
INSERT INTO `country_states` VALUES ('322', 'FI', 'Kainuu', 'Kainuu', '80');
INSERT INTO `country_states` VALUES ('323', 'FI', 'Pohjois-Karjala', 'Pohjois-Karjala', '80');
INSERT INTO `country_states` VALUES ('324', 'FI', 'Pohjois-Savo', 'Pohjois-Savo', '80');
INSERT INTO `country_states` VALUES ('325', 'FI', 'Etelä-Savo', 'Etelä-Savo', '80');
INSERT INTO `country_states` VALUES ('326', 'FI', 'Etelä-Pohjanmaa', 'Etelä-Pohjanmaa', '80');
INSERT INTO `country_states` VALUES ('327', 'FI', 'Pohjanmaa', 'Pohjanmaa', '80');
INSERT INTO `country_states` VALUES ('328', 'FI', 'Pirkanmaa', 'Pirkanmaa', '80');
INSERT INTO `country_states` VALUES ('329', 'FI', 'Satakunta', 'Satakunta', '80');
INSERT INTO `country_states` VALUES ('330', 'FI', 'Keski-Pohjanmaa', 'Keski-Pohjanmaa', '80');
INSERT INTO `country_states` VALUES ('331', 'FI', 'Keski-Suomi', 'Keski-Suomi', '80');
INSERT INTO `country_states` VALUES ('332', 'FI', 'Varsinais-Suomi', 'Varsinais-Suomi', '80');
INSERT INTO `country_states` VALUES ('333', 'FI', 'Etelä-Karjala', 'Etelä-Karjala', '80');
INSERT INTO `country_states` VALUES ('334', 'FI', 'Päijät-Häme', 'Päijät-Häme', '80');
INSERT INTO `country_states` VALUES ('335', 'FI', 'Kanta-Häme', 'Kanta-Häme', '80');
INSERT INTO `country_states` VALUES ('336', 'FI', 'Uusimaa', 'Uusimaa', '80');
INSERT INTO `country_states` VALUES ('337', 'FI', 'Itä-Uusimaa', 'Itä-Uusimaa', '80');
INSERT INTO `country_states` VALUES ('338', 'FI', 'Kymenlaakso', 'Kymenlaakso', '80');
INSERT INTO `country_states` VALUES ('339', 'FI', 'Ahvenanmaa', 'Ahvenanmaa', '80');
INSERT INTO `country_states` VALUES ('340', 'EE', 'EE-37', 'Harjumaa', '74');
INSERT INTO `country_states` VALUES ('341', 'EE', 'EE-39', 'Hiiumaa', '74');
INSERT INTO `country_states` VALUES ('342', 'EE', 'EE-44', 'Ida-Virumaa', '74');
INSERT INTO `country_states` VALUES ('343', 'EE', 'EE-49', 'Jõgevamaa', '74');
INSERT INTO `country_states` VALUES ('344', 'EE', 'EE-51', 'Järvamaa', '74');
INSERT INTO `country_states` VALUES ('345', 'EE', 'EE-57', 'Läänemaa', '74');
INSERT INTO `country_states` VALUES ('346', 'EE', 'EE-59', 'Lääne-Virumaa', '74');
INSERT INTO `country_states` VALUES ('347', 'EE', 'EE-65', 'Põlvamaa', '74');
INSERT INTO `country_states` VALUES ('348', 'EE', 'EE-67', 'Pärnumaa', '74');
INSERT INTO `country_states` VALUES ('349', 'EE', 'EE-70', 'Raplamaa', '74');
INSERT INTO `country_states` VALUES ('350', 'EE', 'EE-74', 'Saaremaa', '74');
INSERT INTO `country_states` VALUES ('351', 'EE', 'EE-78', 'Tartumaa', '74');
INSERT INTO `country_states` VALUES ('352', 'EE', 'EE-82', 'Valgamaa', '74');
INSERT INTO `country_states` VALUES ('353', 'EE', 'EE-84', 'Viljandimaa', '74');
INSERT INTO `country_states` VALUES ('354', 'EE', 'EE-86', 'Võrumaa', '74');
INSERT INTO `country_states` VALUES ('355', 'LV', 'LV-DGV', 'Daugavpils', '125');
INSERT INTO `country_states` VALUES ('356', 'LV', 'LV-JEL', 'Jelgava', '125');
INSERT INTO `country_states` VALUES ('357', 'LV', 'Jēkabpils', 'Jēkabpils', '125');
INSERT INTO `country_states` VALUES ('358', 'LV', 'LV-JUR', 'Jūrmala', '125');
INSERT INTO `country_states` VALUES ('359', 'LV', 'LV-LPX', 'Liepāja', '125');
INSERT INTO `country_states` VALUES ('360', 'LV', 'LV-LE', 'Liepājas novads', '125');
INSERT INTO `country_states` VALUES ('361', 'LV', 'LV-REZ', 'Rēzekne', '125');
INSERT INTO `country_states` VALUES ('362', 'LV', 'LV-RIX', 'Rīga', '125');
INSERT INTO `country_states` VALUES ('363', 'LV', 'LV-RI', 'Rīgas novads', '125');
INSERT INTO `country_states` VALUES ('364', 'LV', 'Valmiera', 'Valmiera', '125');
INSERT INTO `country_states` VALUES ('365', 'LV', 'LV-VEN', 'Ventspils', '125');
INSERT INTO `country_states` VALUES ('366', 'LV', 'Aglonas novads', 'Aglonas novads', '125');
INSERT INTO `country_states` VALUES ('367', 'LV', 'LV-AI', 'Aizkraukles novads', '125');
INSERT INTO `country_states` VALUES ('368', 'LV', 'Aizputes novads', 'Aizputes novads', '125');
INSERT INTO `country_states` VALUES ('369', 'LV', 'Aknīstes novads', 'Aknīstes novads', '125');
INSERT INTO `country_states` VALUES ('370', 'LV', 'Alojas novads', 'Alojas novads', '125');
INSERT INTO `country_states` VALUES ('371', 'LV', 'Alsungas novads', 'Alsungas novads', '125');
INSERT INTO `country_states` VALUES ('372', 'LV', 'LV-AL', 'Alūksnes novads', '125');
INSERT INTO `country_states` VALUES ('373', 'LV', 'Amatas novads', 'Amatas novads', '125');
INSERT INTO `country_states` VALUES ('374', 'LV', 'Apes novads', 'Apes novads', '125');
INSERT INTO `country_states` VALUES ('375', 'LV', 'Auces novads', 'Auces novads', '125');
INSERT INTO `country_states` VALUES ('376', 'LV', 'Babītes novads', 'Babītes novads', '125');
INSERT INTO `country_states` VALUES ('377', 'LV', 'Baldones novads', 'Baldones novads', '125');
INSERT INTO `country_states` VALUES ('378', 'LV', 'Baltinavas novads', 'Baltinavas novads', '125');
INSERT INTO `country_states` VALUES ('379', 'LV', 'LV-BL', 'Balvu novads', '125');
INSERT INTO `country_states` VALUES ('380', 'LV', 'LV-BU', 'Bauskas novads', '125');
INSERT INTO `country_states` VALUES ('381', 'LV', 'Beverīnas novads', 'Beverīnas novads', '125');
INSERT INTO `country_states` VALUES ('382', 'LV', 'Brocēnu novads', 'Brocēnu novads', '125');
INSERT INTO `country_states` VALUES ('383', 'LV', 'Burtnieku novads', 'Burtnieku novads', '125');
INSERT INTO `country_states` VALUES ('384', 'LV', 'Carnikavas novads', 'Carnikavas novads', '125');
INSERT INTO `country_states` VALUES ('385', 'LV', 'Cesvaines novads', 'Cesvaines novads', '125');
INSERT INTO `country_states` VALUES ('386', 'LV', 'Ciblas novads', 'Ciblas novads', '125');
INSERT INTO `country_states` VALUES ('387', 'LV', 'LV-CE', 'Cēsu novads', '125');
INSERT INTO `country_states` VALUES ('388', 'LV', 'Dagdas novads', 'Dagdas novads', '125');
INSERT INTO `country_states` VALUES ('389', 'LV', 'LV-DA', 'Daugavpils novads', '125');
INSERT INTO `country_states` VALUES ('390', 'LV', 'LV-DO', 'Dobeles novads', '125');
INSERT INTO `country_states` VALUES ('391', 'LV', 'Dundagas novads', 'Dundagas novads', '125');
INSERT INTO `country_states` VALUES ('392', 'LV', 'Durbes novads', 'Durbes novads', '125');
INSERT INTO `country_states` VALUES ('393', 'LV', 'Engures novads', 'Engures novads', '125');
INSERT INTO `country_states` VALUES ('394', 'LV', 'Garkalnes novads', 'Garkalnes novads', '125');
INSERT INTO `country_states` VALUES ('395', 'LV', 'Grobiņas novads', 'Grobiņas novads', '125');
INSERT INTO `country_states` VALUES ('396', 'LV', 'LV-GU', 'Gulbenes novads', '125');
INSERT INTO `country_states` VALUES ('397', 'LV', 'Iecavas novads', 'Iecavas novads', '125');
INSERT INTO `country_states` VALUES ('398', 'LV', 'Ikšķiles novads', 'Ikšķiles novads', '125');
INSERT INTO `country_states` VALUES ('399', 'LV', 'Ilūkstes novads', 'Ilūkstes novads', '125');
INSERT INTO `country_states` VALUES ('400', 'LV', 'Inčukalna novads', 'Inčukalna novads', '125');
INSERT INTO `country_states` VALUES ('401', 'LV', 'Jaunjelgavas novads', 'Jaunjelgavas novads', '125');
INSERT INTO `country_states` VALUES ('402', 'LV', 'Jaunpiebalgas novads', 'Jaunpiebalgas novads', '125');
INSERT INTO `country_states` VALUES ('403', 'LV', 'Jaunpils novads', 'Jaunpils novads', '125');
INSERT INTO `country_states` VALUES ('404', 'LV', 'LV-JL', 'Jelgavas novads', '125');
INSERT INTO `country_states` VALUES ('405', 'LV', 'LV-JK', 'Jēkabpils novads', '125');
INSERT INTO `country_states` VALUES ('406', 'LV', 'Kandavas novads', 'Kandavas novads', '125');
INSERT INTO `country_states` VALUES ('407', 'LV', 'Kokneses novads', 'Kokneses novads', '125');
INSERT INTO `country_states` VALUES ('408', 'LV', 'Krimuldas novads', 'Krimuldas novads', '125');
INSERT INTO `country_states` VALUES ('409', 'LV', 'Krustpils novads', 'Krustpils novads', '125');
INSERT INTO `country_states` VALUES ('410', 'LV', 'LV-KR', 'Krāslavas novads', '125');
INSERT INTO `country_states` VALUES ('411', 'LV', 'LV-KU', 'Kuldīgas novads', '125');
INSERT INTO `country_states` VALUES ('412', 'LV', 'Kārsavas novads', 'Kārsavas novads', '125');
INSERT INTO `country_states` VALUES ('413', 'LV', 'Lielvārdes novads', 'Lielvārdes novads', '125');
INSERT INTO `country_states` VALUES ('414', 'LV', 'LV-LM', 'Limbažu novads', '125');
INSERT INTO `country_states` VALUES ('415', 'LV', 'Lubānas novads', 'Lubānas novads', '125');
INSERT INTO `country_states` VALUES ('416', 'LV', 'LV-LU', 'Ludzas novads', '125');
INSERT INTO `country_states` VALUES ('417', 'LV', 'Līgatnes novads', 'Līgatnes novads', '125');
INSERT INTO `country_states` VALUES ('418', 'LV', 'Līvānu novads', 'Līvānu novads', '125');
INSERT INTO `country_states` VALUES ('419', 'LV', 'LV-MA', 'Madonas novads', '125');
INSERT INTO `country_states` VALUES ('420', 'LV', 'Mazsalacas novads', 'Mazsalacas novads', '125');
INSERT INTO `country_states` VALUES ('421', 'LV', 'Mālpils novads', 'Mālpils novads', '125');
INSERT INTO `country_states` VALUES ('422', 'LV', 'Mārupes novads', 'Mārupes novads', '125');
INSERT INTO `country_states` VALUES ('423', 'LV', 'Naukšēnu novads', 'Naukšēnu novads', '125');
INSERT INTO `country_states` VALUES ('424', 'LV', 'Neretas novads', 'Neretas novads', '125');
INSERT INTO `country_states` VALUES ('425', 'LV', 'Nīcas novads', 'Nīcas novads', '125');
INSERT INTO `country_states` VALUES ('426', 'LV', 'LV-OG', 'Ogres novads', '125');
INSERT INTO `country_states` VALUES ('427', 'LV', 'Olaines novads', 'Olaines novads', '125');
INSERT INTO `country_states` VALUES ('428', 'LV', 'Ozolnieku novads', 'Ozolnieku novads', '125');
INSERT INTO `country_states` VALUES ('429', 'LV', 'LV-PR', 'Preiļu novads', '125');
INSERT INTO `country_states` VALUES ('430', 'LV', 'Priekules novads', 'Priekules novads', '125');
INSERT INTO `country_states` VALUES ('431', 'LV', 'Priekuļu novads', 'Priekuļu novads', '125');
INSERT INTO `country_states` VALUES ('432', 'LV', 'Pārgaujas novads', 'Pārgaujas novads', '125');
INSERT INTO `country_states` VALUES ('433', 'LV', 'Pāvilostas novads', 'Pāvilostas novads', '125');
INSERT INTO `country_states` VALUES ('434', 'LV', 'Pļaviņu novads', 'Pļaviņu novads', '125');
INSERT INTO `country_states` VALUES ('435', 'LV', 'Raunas novads', 'Raunas novads', '125');
INSERT INTO `country_states` VALUES ('436', 'LV', 'Riebiņu novads', 'Riebiņu novads', '125');
INSERT INTO `country_states` VALUES ('437', 'LV', 'Rojas novads', 'Rojas novads', '125');
INSERT INTO `country_states` VALUES ('438', 'LV', 'Ropažu novads', 'Ropažu novads', '125');
INSERT INTO `country_states` VALUES ('439', 'LV', 'Rucavas novads', 'Rucavas novads', '125');
INSERT INTO `country_states` VALUES ('440', 'LV', 'Rugāju novads', 'Rugāju novads', '125');
INSERT INTO `country_states` VALUES ('441', 'LV', 'Rundāles novads', 'Rundāles novads', '125');
INSERT INTO `country_states` VALUES ('442', 'LV', 'LV-RE', 'Rēzeknes novads', '125');
INSERT INTO `country_states` VALUES ('443', 'LV', 'Rūjienas novads', 'Rūjienas novads', '125');
INSERT INTO `country_states` VALUES ('444', 'LV', 'Salacgrīvas novads', 'Salacgrīvas novads', '125');
INSERT INTO `country_states` VALUES ('445', 'LV', 'Salas novads', 'Salas novads', '125');
INSERT INTO `country_states` VALUES ('446', 'LV', 'Salaspils novads', 'Salaspils novads', '125');
INSERT INTO `country_states` VALUES ('447', 'LV', 'LV-SA', 'Saldus novads', '125');
INSERT INTO `country_states` VALUES ('448', 'LV', 'Saulkrastu novads', 'Saulkrastu novads', '125');
INSERT INTO `country_states` VALUES ('449', 'LV', 'Siguldas novads', 'Siguldas novads', '125');
INSERT INTO `country_states` VALUES ('450', 'LV', 'Skrundas novads', 'Skrundas novads', '125');
INSERT INTO `country_states` VALUES ('451', 'LV', 'Skrīveru novads', 'Skrīveru novads', '125');
INSERT INTO `country_states` VALUES ('452', 'LV', 'Smiltenes novads', 'Smiltenes novads', '125');
INSERT INTO `country_states` VALUES ('453', 'LV', 'Stopiņu novads', 'Stopiņu novads', '125');
INSERT INTO `country_states` VALUES ('454', 'LV', 'Strenču novads', 'Strenču novads', '125');
INSERT INTO `country_states` VALUES ('455', 'LV', 'Sējas novads', 'Sējas novads', '125');
INSERT INTO `country_states` VALUES ('456', 'LV', 'LV-TA', 'Talsu novads', '125');
INSERT INTO `country_states` VALUES ('457', 'LV', 'LV-TU', 'Tukuma novads', '125');
INSERT INTO `country_states` VALUES ('458', 'LV', 'Tērvetes novads', 'Tērvetes novads', '125');
INSERT INTO `country_states` VALUES ('459', 'LV', 'Vaiņodes novads', 'Vaiņodes novads', '125');
INSERT INTO `country_states` VALUES ('460', 'LV', 'LV-VK', 'Valkas novads', '125');
INSERT INTO `country_states` VALUES ('461', 'LV', 'LV-VM', 'Valmieras novads', '125');
INSERT INTO `country_states` VALUES ('462', 'LV', 'Varakļānu novads', 'Varakļānu novads', '125');
INSERT INTO `country_states` VALUES ('463', 'LV', 'Vecpiebalgas novads', 'Vecpiebalgas novads', '125');
INSERT INTO `country_states` VALUES ('464', 'LV', 'Vecumnieku novads', 'Vecumnieku novads', '125');
INSERT INTO `country_states` VALUES ('465', 'LV', 'LV-VE', 'Ventspils novads', '125');
INSERT INTO `country_states` VALUES ('466', 'LV', 'Viesītes novads', 'Viesītes novads', '125');
INSERT INTO `country_states` VALUES ('467', 'LV', 'Viļakas novads', 'Viļakas novads', '125');
INSERT INTO `country_states` VALUES ('468', 'LV', 'Viļānu novads', 'Viļānu novads', '125');
INSERT INTO `country_states` VALUES ('469', 'LV', 'Vārkavas novads', 'Vārkavas novads', '125');
INSERT INTO `country_states` VALUES ('470', 'LV', 'Zilupes novads', 'Zilupes novads', '125');
INSERT INTO `country_states` VALUES ('471', 'LV', 'Ādažu novads', 'Ādažu novads', '125');
INSERT INTO `country_states` VALUES ('472', 'LV', 'Ērgļu novads', 'Ērgļu novads', '125');
INSERT INTO `country_states` VALUES ('473', 'LV', 'Ķeguma novads', 'Ķeguma novads', '125');
INSERT INTO `country_states` VALUES ('474', 'LV', 'Ķekavas novads', 'Ķekavas novads', '125');
INSERT INTO `country_states` VALUES ('475', 'LT', 'LT-AL', 'Alytaus Apskritis', '131');
INSERT INTO `country_states` VALUES ('476', 'LT', 'LT-KU', 'Kauno Apskritis', '131');
INSERT INTO `country_states` VALUES ('477', 'LT', 'LT-KL', 'Klaipėdos Apskritis', '131');
INSERT INTO `country_states` VALUES ('478', 'LT', 'LT-MR', 'Marijampolės Apskritis', '131');
INSERT INTO `country_states` VALUES ('479', 'LT', 'LT-PN', 'Panevėžio Apskritis', '131');
INSERT INTO `country_states` VALUES ('480', 'LT', 'LT-SA', 'Šiaulių Apskritis', '131');
INSERT INTO `country_states` VALUES ('481', 'LT', 'LT-TA', 'Tauragės Apskritis', '131');
INSERT INTO `country_states` VALUES ('482', 'LT', 'LT-TE', 'Telšių Apskritis', '131');
INSERT INTO `country_states` VALUES ('483', 'LT', 'LT-UT', 'Utenos Apskritis', '131');
INSERT INTO `country_states` VALUES ('484', 'LT', 'LT-VL', 'Vilniaus Apskritis', '131');
INSERT INTO `country_states` VALUES ('485', 'BR', 'AC', 'Acre', '31');
INSERT INTO `country_states` VALUES ('486', 'BR', 'AL', 'Alagoas', '31');
INSERT INTO `country_states` VALUES ('487', 'BR', 'AP', 'Amapá', '31');
INSERT INTO `country_states` VALUES ('488', 'BR', 'AM', 'Amazonas', '31');
INSERT INTO `country_states` VALUES ('489', 'BR', 'BA', 'Bahia', '31');
INSERT INTO `country_states` VALUES ('490', 'BR', 'CE', 'Ceará', '31');
INSERT INTO `country_states` VALUES ('491', 'BR', 'ES', 'Espírito Santo', '31');
INSERT INTO `country_states` VALUES ('492', 'BR', 'GO', 'Goiás', '31');
INSERT INTO `country_states` VALUES ('493', 'BR', 'MA', 'Maranhão', '31');
INSERT INTO `country_states` VALUES ('494', 'BR', 'MT', 'Mato Grosso', '31');
INSERT INTO `country_states` VALUES ('495', 'BR', 'MS', 'Mato Grosso do Sul', '31');
INSERT INTO `country_states` VALUES ('496', 'BR', 'MG', 'Minas Gerais', '31');
INSERT INTO `country_states` VALUES ('497', 'BR', 'PA', 'Pará', '31');
INSERT INTO `country_states` VALUES ('498', 'BR', 'PB', 'Paraíba', '31');
INSERT INTO `country_states` VALUES ('499', 'BR', 'PR', 'Paraná', '31');
INSERT INTO `country_states` VALUES ('500', 'BR', 'PE', 'Pernambuco', '31');
INSERT INTO `country_states` VALUES ('501', 'BR', 'PI', 'Piauí', '31');
INSERT INTO `country_states` VALUES ('502', 'BR', 'RJ', 'Rio de Janeiro', '31');
INSERT INTO `country_states` VALUES ('503', 'BR', 'RN', 'Rio Grande do Norte', '31');
INSERT INTO `country_states` VALUES ('504', 'BR', 'RS', 'Rio Grande do Sul', '31');
INSERT INTO `country_states` VALUES ('505', 'BR', 'RO', 'Rondônia', '31');
INSERT INTO `country_states` VALUES ('506', 'BR', 'RR', 'Roraima', '31');
INSERT INTO `country_states` VALUES ('507', 'BR', 'SC', 'Santa Catarina', '31');
INSERT INTO `country_states` VALUES ('508', 'BR', 'SP', 'São Paulo', '31');
INSERT INTO `country_states` VALUES ('509', 'BR', 'SE', 'Sergipe', '31');
INSERT INTO `country_states` VALUES ('510', 'BR', 'TO', 'Tocantins', '31');
INSERT INTO `country_states` VALUES ('511', 'BR', 'DF', 'Distrito Federal', '31');
INSERT INTO `country_states` VALUES ('512', 'HR', 'HR-01', 'Zagrebačka županija', '59');
INSERT INTO `country_states` VALUES ('513', 'HR', 'HR-02', 'Krapinsko-zagorska županija', '59');
INSERT INTO `country_states` VALUES ('514', 'HR', 'HR-03', 'Sisačko-moslavačka županija', '59');
INSERT INTO `country_states` VALUES ('515', 'HR', 'HR-04', 'Karlovačka županija', '59');
INSERT INTO `country_states` VALUES ('516', 'HR', 'HR-05', 'Varaždinska županija', '59');
INSERT INTO `country_states` VALUES ('517', 'HR', 'HR-06', 'Koprivničko-križevačka županija', '59');
INSERT INTO `country_states` VALUES ('518', 'HR', 'HR-07', 'Bjelovarsko-bilogorska županija', '59');
INSERT INTO `country_states` VALUES ('519', 'HR', 'HR-08', 'Primorsko-goranska županija', '59');
INSERT INTO `country_states` VALUES ('520', 'HR', 'HR-09', 'Ličko-senjska županija', '59');
INSERT INTO `country_states` VALUES ('521', 'HR', 'HR-10', 'Virovitičko-podravska županija', '59');
INSERT INTO `country_states` VALUES ('522', 'HR', 'HR-11', 'Požeško-slavonska županija', '59');
INSERT INTO `country_states` VALUES ('523', 'HR', 'HR-12', 'Brodsko-posavska županija', '59');
INSERT INTO `country_states` VALUES ('524', 'HR', 'HR-13', 'Zadarska županija', '59');
INSERT INTO `country_states` VALUES ('525', 'HR', 'HR-14', 'Osječko-baranjska županija', '59');
INSERT INTO `country_states` VALUES ('526', 'HR', 'HR-15', 'Šibensko-kninska županija', '59');
INSERT INTO `country_states` VALUES ('527', 'HR', 'HR-16', 'Vukovarsko-srijemska županija', '59');
INSERT INTO `country_states` VALUES ('528', 'HR', 'HR-17', 'Splitsko-dalmatinska županija', '59');
INSERT INTO `country_states` VALUES ('529', 'HR', 'HR-18', 'Istarska županija', '59');
INSERT INTO `country_states` VALUES ('530', 'HR', 'HR-19', 'Dubrovačko-neretvanska županija', '59');
INSERT INTO `country_states` VALUES ('531', 'HR', 'HR-20', 'Međimurska županija', '59');
INSERT INTO `country_states` VALUES ('532', 'HR', 'HR-21', 'Grad Zagreb', '59');
INSERT INTO `country_states` VALUES ('533', 'IN', 'AN', 'Andaman and Nicobar Islands', '106');
INSERT INTO `country_states` VALUES ('534', 'IN', 'AP', 'Andhra Pradesh', '106');
INSERT INTO `country_states` VALUES ('535', 'IN', 'AR', 'Arunachal Pradesh', '106');
INSERT INTO `country_states` VALUES ('536', 'IN', 'AS', 'Assam', '106');
INSERT INTO `country_states` VALUES ('537', 'IN', 'BR', 'Bihar', '106');
INSERT INTO `country_states` VALUES ('538', 'IN', 'CH', 'Chandigarh', '106');
INSERT INTO `country_states` VALUES ('539', 'IN', 'CT', 'Chhattisgarh', '106');
INSERT INTO `country_states` VALUES ('540', 'IN', 'DN', 'Dadra and Nagar Haveli', '106');
INSERT INTO `country_states` VALUES ('541', 'IN', 'DD', 'Daman and Diu', '106');
INSERT INTO `country_states` VALUES ('542', 'IN', 'DL', 'Delhi', '106');
INSERT INTO `country_states` VALUES ('543', 'IN', 'GA', 'Goa', '106');
INSERT INTO `country_states` VALUES ('544', 'IN', 'GJ', 'Gujarat', '106');
INSERT INTO `country_states` VALUES ('545', 'IN', 'HR', 'Haryana', '106');
INSERT INTO `country_states` VALUES ('546', 'IN', 'HP', 'Himachal Pradesh', '106');
INSERT INTO `country_states` VALUES ('547', 'IN', 'JK', 'Jammu and Kashmir', '106');
INSERT INTO `country_states` VALUES ('548', 'IN', 'JH', 'Jharkhand', '106');
INSERT INTO `country_states` VALUES ('549', 'IN', 'KA', 'Karnataka', '106');
INSERT INTO `country_states` VALUES ('550', 'IN', 'KL', 'Kerala', '106');
INSERT INTO `country_states` VALUES ('551', 'IN', 'LD', 'Lakshadweep', '106');
INSERT INTO `country_states` VALUES ('552', 'IN', 'MP', 'Madhya Pradesh', '106');
INSERT INTO `country_states` VALUES ('553', 'IN', 'MH', 'Maharashtra', '106');
INSERT INTO `country_states` VALUES ('554', 'IN', 'MN', 'Manipur', '106');
INSERT INTO `country_states` VALUES ('555', 'IN', 'ML', 'Meghalaya', '106');
INSERT INTO `country_states` VALUES ('556', 'IN', 'MZ', 'Mizoram', '106');
INSERT INTO `country_states` VALUES ('557', 'IN', 'NL', 'Nagaland', '106');
INSERT INTO `country_states` VALUES ('558', 'IN', 'OR', 'Odisha', '106');
INSERT INTO `country_states` VALUES ('559', 'IN', 'PY', 'Puducherry', '106');
INSERT INTO `country_states` VALUES ('560', 'IN', 'PB', 'Punjab', '106');
INSERT INTO `country_states` VALUES ('561', 'IN', 'RJ', 'Rajasthan', '106');
INSERT INTO `country_states` VALUES ('562', 'IN', 'SK', 'Sikkim', '106');
INSERT INTO `country_states` VALUES ('563', 'IN', 'TN', 'Tamil Nadu', '106');
INSERT INTO `country_states` VALUES ('564', 'IN', 'TG', 'Telangana', '106');
INSERT INTO `country_states` VALUES ('565', 'IN', 'TR', 'Tripura', '106');
INSERT INTO `country_states` VALUES ('566', 'IN', 'UP', 'Uttar Pradesh', '106');
INSERT INTO `country_states` VALUES ('567', 'IN', 'UT', 'Uttarakhand', '106');
INSERT INTO `country_states` VALUES ('568', 'IN', 'WB', 'West Bengal', '106');

-- ----------------------------
-- Table structure for country_state_translations
-- ----------------------------
DROP TABLE IF EXISTS `country_state_translations`;
CREATE TABLE `country_state_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_state_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_state_translations_country_state_id_foreign` (`country_state_id`),
  CONSTRAINT `country_state_translations_country_state_id_foreign` FOREIGN KEY (`country_state_id`) REFERENCES `country_states` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of country_state_translations
-- ----------------------------

-- ----------------------------
-- Table structure for country_translations
-- ----------------------------
DROP TABLE IF EXISTS `country_translations`;
CREATE TABLE `country_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_translations_country_id_foreign` (`country_id`),
  CONSTRAINT `country_translations_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of country_translations
-- ----------------------------

-- ----------------------------
-- Table structure for currencies
-- ----------------------------
DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of currencies
-- ----------------------------
INSERT INTO `currencies` VALUES ('1', 'USD', 'US Dollar', null, null);

-- ----------------------------
-- Table structure for currency_exchange_rates
-- ----------------------------
DROP TABLE IF EXISTS `currency_exchange_rates`;
CREATE TABLE `currency_exchange_rates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rate` decimal(10,5) NOT NULL,
  `target_currency` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `currency_exchange_rates_target_currency_unique` (`target_currency`),
  CONSTRAINT `currency_exchange_rates_target_currency_foreign` FOREIGN KEY (`target_currency`) REFERENCES `currencies` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of currency_exchange_rates
-- ----------------------------

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(10) unsigned NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_group_id` int(10) unsigned DEFAULT NULL,
  `subscribed_to_news_letter` tinyint(1) NOT NULL DEFAULT 0,
  `is_verified` tinyint(1) NOT NULL DEFAULT 0,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_email_unique` (`email`),
  KEY `customers_channel_id_foreign` (`channel_id`),
  KEY `customers_customer_group_id_foreign` (`customer_group_id`),
  CONSTRAINT `customers_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`),
  CONSTRAINT `customers_customer_group_id_foreign` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_groups` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES ('2', '1', 'khanhpoly', 'khanhpoly', null, null, 'khanhpoly@gmail.com', '1', '$2y$10$YZsAtVdeENCcYaOGZEea..o0l5tFsY4lh1nDm6oESck5wcQ7awgu6', '2', '0', '1', '2f2e170296d89893af87751c11048545', null, '2019-10-02 14:21:13', '2019-10-02 14:21:13', null, null);

-- ----------------------------
-- Table structure for customer_addresses
-- ----------------------------
DROP TABLE IF EXISTS `customer_addresses`;
CREATE TABLE `customer_addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` int(11) NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_address` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_addresses_customer_id_foreign` (`customer_id`),
  CONSTRAINT `customer_addresses_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of customer_addresses
-- ----------------------------
INSERT INTO `customer_addresses` VALUES ('2', '2', 'ct4 chung cư the pride, tố hữu hà đông', null, '', '', 'Quận Hà Đông', '10000', '968667361', '1', null, '2019-10-02 14:44:18', '2019-10-02 14:44:18');

-- ----------------------------
-- Table structure for customer_groups
-- ----------------------------
DROP TABLE IF EXISTS `customer_groups`;
CREATE TABLE `customer_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_user_defined` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_groups_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of customer_groups
-- ----------------------------
INSERT INTO `customer_groups` VALUES ('1', 'Guest', '0', null, null, 'guest');
INSERT INTO `customer_groups` VALUES ('2', 'General', '0', null, null, 'general');
INSERT INTO `customer_groups` VALUES ('3', 'Wholesale', '0', null, null, 'wholesale');

-- ----------------------------
-- Table structure for customer_password_resets
-- ----------------------------
DROP TABLE IF EXISTS `customer_password_resets`;
CREATE TABLE `customer_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `customer_password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of customer_password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for inventory_sources
-- ----------------------------
DROP TABLE IF EXISTS `inventory_sources`;
CREATE TABLE `inventory_sources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL DEFAULT 0,
  `latitude` decimal(10,5) DEFAULT NULL,
  `longitude` decimal(10,5) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `inventory_sources_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of inventory_sources
-- ----------------------------
INSERT INTO `inventory_sources` VALUES ('1', 'default', 'Default', null, 'Detroit Warehouse', 'warehouse@example.com', '1234567899', null, 'US', 'MI', 'Detroit', '12th Street', '48127', '0', null, null, '1', null, null);

-- ----------------------------
-- Table structure for invoices
-- ----------------------------
DROP TABLE IF EXISTS `invoices`;
CREATE TABLE `invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `increment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_sent` tinyint(1) NOT NULL DEFAULT 0,
  `total_qty` int(11) DEFAULT NULL,
  `base_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_total` decimal(12,4) DEFAULT 0.0000,
  `base_sub_total` decimal(12,4) DEFAULT 0.0000,
  `grand_total` decimal(12,4) DEFAULT 0.0000,
  `base_grand_total` decimal(12,4) DEFAULT 0.0000,
  `shipping_amount` decimal(12,4) DEFAULT 0.0000,
  `base_shipping_amount` decimal(12,4) DEFAULT 0.0000,
  `tax_amount` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount` decimal(12,4) DEFAULT 0.0000,
  `discount_amount` decimal(12,4) DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) DEFAULT 0.0000,
  `order_id` int(10) unsigned DEFAULT NULL,
  `order_address_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoices_order_id_foreign` (`order_id`),
  KEY `invoices_order_address_id_foreign` (`order_address_id`),
  CONSTRAINT `invoices_order_address_id_foreign` FOREIGN KEY (`order_address_id`) REFERENCES `order_address` (`id`) ON DELETE SET NULL,
  CONSTRAINT `invoices_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of invoices
-- ----------------------------

-- ----------------------------
-- Table structure for invoice_items
-- ----------------------------
DROP TABLE IF EXISTS `invoice_items`;
CREATE TABLE `invoice_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `total` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_total` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `tax_amount` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount` decimal(12,4) DEFAULT 0.0000,
  `product_id` int(10) unsigned DEFAULT NULL,
  `product_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_item_id` int(10) unsigned DEFAULT NULL,
  `invoice_id` int(10) unsigned DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `additional` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `discount_percent` decimal(12,4) DEFAULT 0.0000,
  `discount_amount` decimal(12,4) DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) DEFAULT 0.0000,
  PRIMARY KEY (`id`),
  KEY `invoice_items_invoice_id_foreign` (`invoice_id`),
  KEY `invoice_items_parent_id_foreign` (`parent_id`),
  CONSTRAINT `invoice_items_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE,
  CONSTRAINT `invoice_items_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `invoice_items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of invoice_items
-- ----------------------------

-- ----------------------------
-- Table structure for locales
-- ----------------------------
DROP TABLE IF EXISTS `locales`;
CREATE TABLE `locales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `direction` enum('ltr','rtl') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ltr',
  PRIMARY KEY (`id`),
  UNIQUE KEY `locales_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of locales
-- ----------------------------
INSERT INTO `locales` VALUES ('1', 'en', 'English', null, null, 'ltr');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_admin_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('4', '2018_06_12_111907_create_admins_table', '1');
INSERT INTO `migrations` VALUES ('5', '2018_06_13_055341_create_roles_table', '1');
INSERT INTO `migrations` VALUES ('6', '2018_07_05_130148_create_attributes_table', '1');
INSERT INTO `migrations` VALUES ('7', '2018_07_05_132854_create_attribute_translations_table', '1');
INSERT INTO `migrations` VALUES ('8', '2018_07_05_135150_create_attribute_families_table', '1');
INSERT INTO `migrations` VALUES ('9', '2018_07_05_135152_create_attribute_groups_table', '1');
INSERT INTO `migrations` VALUES ('10', '2018_07_05_140832_create_attribute_options_table', '1');
INSERT INTO `migrations` VALUES ('11', '2018_07_05_140856_create_attribute_option_translations_table', '1');
INSERT INTO `migrations` VALUES ('12', '2018_07_05_142820_create_categories_table', '1');
INSERT INTO `migrations` VALUES ('13', '2018_07_10_055143_create_locales_table', '1');
INSERT INTO `migrations` VALUES ('14', '2018_07_20_054426_create_countries_table', '1');
INSERT INTO `migrations` VALUES ('15', '2018_07_20_054502_create_currencies_table', '1');
INSERT INTO `migrations` VALUES ('16', '2018_07_20_054542_create_currency_exchange_rates_table', '1');
INSERT INTO `migrations` VALUES ('17', '2018_07_20_064849_create_channels_table', '1');
INSERT INTO `migrations` VALUES ('18', '2018_07_21_142836_create_category_translations_table', '1');
INSERT INTO `migrations` VALUES ('19', '2018_07_23_110040_create_inventory_sources_table', '1');
INSERT INTO `migrations` VALUES ('20', '2018_07_24_082635_create_customer_groups_table', '1');
INSERT INTO `migrations` VALUES ('21', '2018_07_24_082930_create_customers_table', '1');
INSERT INTO `migrations` VALUES ('22', '2018_07_24_083025_create_customer_addresses_table', '1');
INSERT INTO `migrations` VALUES ('23', '2018_07_27_065727_create_products_table', '1');
INSERT INTO `migrations` VALUES ('24', '2018_07_27_070011_create_product_attribute_values_table', '1');
INSERT INTO `migrations` VALUES ('25', '2018_07_27_092623_create_product_reviews_table', '1');
INSERT INTO `migrations` VALUES ('26', '2018_07_27_113941_create_product_images_table', '1');
INSERT INTO `migrations` VALUES ('27', '2018_07_27_113956_create_product_inventories_table', '1');
INSERT INTO `migrations` VALUES ('28', '2018_08_03_114203_create_sliders_table', '1');
INSERT INTO `migrations` VALUES ('29', '2018_08_30_064755_create_tax_categories_table', '1');
INSERT INTO `migrations` VALUES ('30', '2018_08_30_065042_create_tax_rates_table', '1');
INSERT INTO `migrations` VALUES ('31', '2018_08_30_065840_create_tax_mappings_table', '1');
INSERT INTO `migrations` VALUES ('32', '2018_09_05_150444_create_cart_table', '1');
INSERT INTO `migrations` VALUES ('33', '2018_09_05_150915_create_cart_items_table', '1');
INSERT INTO `migrations` VALUES ('34', '2018_09_11_064045_customer_password_resets', '1');
INSERT INTO `migrations` VALUES ('35', '2018_09_19_092845_create_cart_address', '1');
INSERT INTO `migrations` VALUES ('36', '2018_09_19_093453_create_cart_payment', '1');
INSERT INTO `migrations` VALUES ('37', '2018_09_19_093508_create_cart_shipping_rates_table', '1');
INSERT INTO `migrations` VALUES ('38', '2018_09_20_060658_create_core_config_table', '1');
INSERT INTO `migrations` VALUES ('39', '2018_09_27_113154_create_orders_table', '1');
INSERT INTO `migrations` VALUES ('40', '2018_09_27_113207_create_order_items_table', '1');
INSERT INTO `migrations` VALUES ('41', '2018_09_27_113405_create_order_address_table', '1');
INSERT INTO `migrations` VALUES ('42', '2018_09_27_115022_create_shipments_table', '1');
INSERT INTO `migrations` VALUES ('43', '2018_09_27_115029_create_shipment_items_table', '1');
INSERT INTO `migrations` VALUES ('44', '2018_09_27_115135_create_invoices_table', '1');
INSERT INTO `migrations` VALUES ('45', '2018_09_27_115144_create_invoice_items_table', '1');
INSERT INTO `migrations` VALUES ('46', '2018_10_01_095504_create_order_payment_table', '1');
INSERT INTO `migrations` VALUES ('47', '2018_10_03_025230_create_wishlist_table', '1');
INSERT INTO `migrations` VALUES ('48', '2018_10_12_101803_create_country_translations_table', '1');
INSERT INTO `migrations` VALUES ('49', '2018_10_12_101913_create_country_states_table', '1');
INSERT INTO `migrations` VALUES ('50', '2018_10_12_101923_create_country_state_translations_table', '1');
INSERT INTO `migrations` VALUES ('51', '2018_11_15_153257_alter_order_table', '1');
INSERT INTO `migrations` VALUES ('52', '2018_11_15_163729_alter_invoice_table', '1');
INSERT INTO `migrations` VALUES ('53', '2018_11_16_173504_create_subscribers_list_table', '1');
INSERT INTO `migrations` VALUES ('54', '2018_11_17_165758_add_is_verified_column_in_customers_table', '1');
INSERT INTO `migrations` VALUES ('55', '2018_11_21_144411_create_cart_item_inventories_table', '1');
INSERT INTO `migrations` VALUES ('56', '2018_11_26_110500_change_gender_column_in_customers_table', '1');
INSERT INTO `migrations` VALUES ('57', '2018_11_27_174449_change_content_column_in_sliders_table', '1');
INSERT INTO `migrations` VALUES ('58', '2018_12_05_132625_drop_foreign_key_core_config_table', '1');
INSERT INTO `migrations` VALUES ('59', '2018_12_05_132629_alter_core_config_table', '1');
INSERT INTO `migrations` VALUES ('60', '2018_12_06_185202_create_product_flat_table', '1');
INSERT INTO `migrations` VALUES ('61', '2018_12_21_101307_alter_channels_table', '1');
INSERT INTO `migrations` VALUES ('62', '2018_12_24_123812_create_channel_inventory_sources_table', '1');
INSERT INTO `migrations` VALUES ('63', '2018_12_24_184402_alter_shipments_table', '1');
INSERT INTO `migrations` VALUES ('64', '2018_12_26_165327_create_product_ordered_inventories_table', '1');
INSERT INTO `migrations` VALUES ('65', '2018_12_31_161114_alter_channels_category_table', '1');
INSERT INTO `migrations` VALUES ('66', '2019_01_11_122452_add_vendor_id_column_in_product_inventories_table', '1');
INSERT INTO `migrations` VALUES ('67', '2019_01_25_124522_add_updated_at_column_in_product_flat_table', '1');
INSERT INTO `migrations` VALUES ('68', '2019_01_29_123053_add_min_price_and_max_price_column_in_product_flat_table', '1');
INSERT INTO `migrations` VALUES ('69', '2019_01_31_164117_update_value_column_type_to_text_in_core_config_table', '1');
INSERT INTO `migrations` VALUES ('70', '2019_02_21_145238_alter_product_reviews_table', '1');
INSERT INTO `migrations` VALUES ('71', '2019_02_21_152709_add_swatch_type_column_in_attributes_table', '1');
INSERT INTO `migrations` VALUES ('72', '2019_02_21_153035_alter_customer_id_in_product_reviews_table', '1');
INSERT INTO `migrations` VALUES ('73', '2019_02_21_153851_add_swatch_value_columns_in_attribute_options_table', '1');
INSERT INTO `migrations` VALUES ('74', '2019_03_15_123337_add_display_mode_column_in_categories_table', '1');
INSERT INTO `migrations` VALUES ('75', '2019_03_28_103658_add_notes_column_in_customers_table', '1');
INSERT INTO `migrations` VALUES ('76', '2019_04_24_155820_alter_product_flat_table', '1');
INSERT INTO `migrations` VALUES ('77', '2019_05_13_024326_create_cart_rule_table', '1');
INSERT INTO `migrations` VALUES ('78', '2019_05_13_024340_create_cart_rule_channels_table', '1');
INSERT INTO `migrations` VALUES ('79', '2019_05_13_024419_create_cart_rule_customer_groups_table', '1');
INSERT INTO `migrations` VALUES ('80', '2019_05_16_094238_create_cart_rule_labels_table', '1');
INSERT INTO `migrations` VALUES ('81', '2019_05_22_165833_update_zipcode_column_type_to_varchar_in_cart_address_table', '1');
INSERT INTO `migrations` VALUES ('82', '2019_05_23_113407_add_remaining_column_in_product_flat_table', '1');
INSERT INTO `migrations` VALUES ('83', '2019_05_23_155520_add_discount_columns_in_invoice_items_table', '1');
INSERT INTO `migrations` VALUES ('84', '2019_05_23_175727_create_cart_rule_customers_table', '1');
INSERT INTO `migrations` VALUES ('85', '2019_05_23_180457_create_cart_rule_coupons_table', '1');
INSERT INTO `migrations` VALUES ('86', '2019_05_23_184029_rename_discount_columns_in_cart_table', '1');
INSERT INTO `migrations` VALUES ('87', '2019_05_24_113949_create_cart_rule_coupons_usage_table', '1');
INSERT INTO `migrations` VALUES ('88', '2019_05_30_141207_create_cart_rule_cart_table', '1');
INSERT INTO `migrations` VALUES ('89', '2019_06_04_114009_add_phone_column_in_customers_table', '1');
INSERT INTO `migrations` VALUES ('90', '2019_06_06_195905_update_custom_price_to_nullable_in_cart_items', '1');
INSERT INTO `migrations` VALUES ('91', '2019_06_15_183412_add_code_column_in_customer_groups_table', '1');
INSERT INTO `migrations` VALUES ('92', '2019_06_19_162817_remove_unique_in_phone_column_in_customers_table', '1');
INSERT INTO `migrations` VALUES ('93', '2019_06_25_110122_remove_is_guest_from_cart_rules_table', '1');
INSERT INTO `migrations` VALUES ('94', '2019_07_11_151210_add_locale_id_in_category_translations', '1');
INSERT INTO `migrations` VALUES ('95', '2019_07_14_070809_add_products_selection_column_in_cart_rules_table', '1');
INSERT INTO `migrations` VALUES ('96', '2019_07_23_033128_alter_locales_table', '1');
INSERT INTO `migrations` VALUES ('97', '2019_07_29_142734_add_use_in_flat_column_in_attributes_table', '1');
INSERT INTO `migrations` VALUES ('98', '2019_07_30_153530_create_cms_pages_table', '1');
INSERT INTO `migrations` VALUES ('99', '2019_07_31_143339_create_category_filterable_attributes_table', '1');
INSERT INTO `migrations` VALUES ('100', '2019_08_08_130433_create_catalog_rules_table', '1');
INSERT INTO `migrations` VALUES ('101', '2019_08_08_130451_create_catalog_rule_customer_groups_table', '1');
INSERT INTO `migrations` VALUES ('102', '2019_08_08_130458_create_catalog_rule_channels_table', '1');
INSERT INTO `migrations` VALUES ('103', '2019_08_08_130550_create_catalog_rule_products_table', '1');
INSERT INTO `migrations` VALUES ('104', '2019_08_08_130583_create_catalog_rule_products_price_table', '1');
INSERT INTO `migrations` VALUES ('105', '2019_08_21_123707_add_seo_column_in_channels_table', '1');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `increment_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_guest` tinyint(1) DEFAULT NULL,
  `customer_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_gift` tinyint(1) NOT NULL DEFAULT 0,
  `total_item_count` int(11) DEFAULT NULL,
  `total_qty_ordered` int(11) DEFAULT NULL,
  `base_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grand_total` decimal(12,4) DEFAULT 0.0000,
  `base_grand_total` decimal(12,4) DEFAULT 0.0000,
  `grand_total_invoiced` decimal(12,4) DEFAULT 0.0000,
  `base_grand_total_invoiced` decimal(12,4) DEFAULT 0.0000,
  `grand_total_refunded` decimal(12,4) DEFAULT 0.0000,
  `base_grand_total_refunded` decimal(12,4) DEFAULT 0.0000,
  `sub_total` decimal(12,4) DEFAULT 0.0000,
  `base_sub_total` decimal(12,4) DEFAULT 0.0000,
  `sub_total_invoiced` decimal(12,4) DEFAULT 0.0000,
  `base_sub_total_invoiced` decimal(12,4) DEFAULT 0.0000,
  `sub_total_refunded` decimal(12,4) DEFAULT 0.0000,
  `base_sub_total_refunded` decimal(12,4) DEFAULT 0.0000,
  `discount_percent` decimal(12,4) DEFAULT 0.0000,
  `discount_amount` decimal(12,4) DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) DEFAULT 0.0000,
  `discount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `base_discount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `discount_refunded` decimal(12,4) DEFAULT 0.0000,
  `base_discount_refunded` decimal(12,4) DEFAULT 0.0000,
  `tax_amount` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount` decimal(12,4) DEFAULT 0.0000,
  `tax_amount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `tax_amount_refunded` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount_refunded` decimal(12,4) DEFAULT 0.0000,
  `shipping_amount` decimal(12,4) DEFAULT 0.0000,
  `base_shipping_amount` decimal(12,4) DEFAULT 0.0000,
  `shipping_invoiced` decimal(12,4) DEFAULT 0.0000,
  `base_shipping_invoiced` decimal(12,4) DEFAULT 0.0000,
  `shipping_refunded` decimal(12,4) DEFAULT 0.0000,
  `base_shipping_refunded` decimal(12,4) DEFAULT 0.0000,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `customer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_id` int(10) unsigned DEFAULT NULL,
  `channel_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cart_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_customer_id_foreign` (`customer_id`),
  KEY `orders_channel_id_foreign` (`channel_id`),
  CONSTRAINT `orders_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE SET NULL,
  CONSTRAINT `orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of orders
-- ----------------------------

-- ----------------------------
-- Table structure for order_address
-- ----------------------------
DROP TABLE IF EXISTS `order_address`;
CREATE TABLE `order_address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` int(11) NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_address_order_id_foreign` (`order_id`),
  KEY `order_address_customer_id_foreign` (`customer_id`),
  CONSTRAINT `order_address_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE SET NULL,
  CONSTRAINT `order_address_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of order_address
-- ----------------------------

-- ----------------------------
-- Table structure for order_items
-- ----------------------------
DROP TABLE IF EXISTS `order_items`;
CREATE TABLE `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` decimal(12,4) DEFAULT 0.0000,
  `total_weight` decimal(12,4) DEFAULT 0.0000,
  `qty_ordered` int(11) DEFAULT 0,
  `qty_shipped` int(11) DEFAULT 0,
  `qty_invoiced` int(11) DEFAULT 0,
  `qty_canceled` int(11) DEFAULT 0,
  `qty_refunded` int(11) DEFAULT 0,
  `price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `total` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_total` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `total_invoiced` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_total_invoiced` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `amount_refunded` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_amount_refunded` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `discount_percent` decimal(12,4) DEFAULT 0.0000,
  `discount_amount` decimal(12,4) DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) DEFAULT 0.0000,
  `discount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `base_discount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `discount_refunded` decimal(12,4) DEFAULT 0.0000,
  `base_discount_refunded` decimal(12,4) DEFAULT 0.0000,
  `tax_percent` decimal(12,4) DEFAULT 0.0000,
  `tax_amount` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount` decimal(12,4) DEFAULT 0.0000,
  `tax_amount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `tax_amount_refunded` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount_refunded` decimal(12,4) DEFAULT 0.0000,
  `product_id` int(10) unsigned DEFAULT NULL,
  `product_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(10) unsigned DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `additional` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_items_order_id_foreign` (`order_id`),
  KEY `order_items_parent_id_foreign` (`parent_id`),
  CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `order_items_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `order_items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of order_items
-- ----------------------------

-- ----------------------------
-- Table structure for order_payment
-- ----------------------------
DROP TABLE IF EXISTS `order_payment`;
CREATE TABLE `order_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_payment_order_id_foreign` (`order_id`),
  CONSTRAINT `order_payment_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of order_payment
-- ----------------------------

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `attribute_family_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_sku_unique` (`sku`),
  KEY `products_attribute_family_id_foreign` (`attribute_family_id`),
  KEY `products_parent_id_foreign` (`parent_id`),
  CONSTRAINT `products_attribute_family_id_foreign` FOREIGN KEY (`attribute_family_id`) REFERENCES `attribute_families` (`id`),
  CONSTRAINT `products_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of products
-- ----------------------------

-- ----------------------------
-- Table structure for product_attribute_values
-- ----------------------------
DROP TABLE IF EXISTS `product_attribute_values`;
CREATE TABLE `product_attribute_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boolean_value` tinyint(1) DEFAULT NULL,
  `integer_value` int(11) DEFAULT NULL,
  `float_value` double DEFAULT NULL,
  `datetime_value` datetime DEFAULT NULL,
  `date_value` date DEFAULT NULL,
  `json_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `chanel_locale_attribute_value_index_unique` (`channel`,`locale`,`attribute_id`,`product_id`),
  KEY `product_attribute_values_product_id_foreign` (`product_id`),
  KEY `product_attribute_values_attribute_id_foreign` (`attribute_id`),
  CONSTRAINT `product_attribute_values_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_attribute_values_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product_attribute_values
-- ----------------------------

-- ----------------------------
-- Table structure for product_categories
-- ----------------------------
DROP TABLE IF EXISTS `product_categories`;
CREATE TABLE `product_categories` (
  `product_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  KEY `product_categories_product_id_foreign` (`product_id`),
  KEY `product_categories_category_id_foreign` (`category_id`),
  CONSTRAINT `product_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_categories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product_categories
-- ----------------------------

-- ----------------------------
-- Table structure for product_cross_sells
-- ----------------------------
DROP TABLE IF EXISTS `product_cross_sells`;
CREATE TABLE `product_cross_sells` (
  `parent_id` int(10) unsigned NOT NULL,
  `child_id` int(10) unsigned NOT NULL,
  KEY `product_cross_sells_parent_id_foreign` (`parent_id`),
  KEY `product_cross_sells_child_id_foreign` (`child_id`),
  CONSTRAINT `product_cross_sells_child_id_foreign` FOREIGN KEY (`child_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_cross_sells_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product_cross_sells
-- ----------------------------

-- ----------------------------
-- Table structure for product_flat
-- ----------------------------
DROP TABLE IF EXISTS `product_flat`;
CREATE TABLE `product_flat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new` tinyint(1) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(12,4) DEFAULT NULL,
  `cost` decimal(12,4) DEFAULT NULL,
  `special_price` decimal(12,4) DEFAULT NULL,
  `special_price_from` date DEFAULT NULL,
  `special_price_to` date DEFAULT NULL,
  `weight` decimal(12,4) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `color_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `size_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `visible_individually` tinyint(1) DEFAULT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` decimal(12,4) DEFAULT NULL,
  `height` decimal(12,4) DEFAULT NULL,
  `depth` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_flat_unique_index` (`product_id`,`channel`,`locale`),
  KEY `product_flat_parent_id_foreign` (`parent_id`),
  CONSTRAINT `product_flat_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `product_flat` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_flat_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product_flat
-- ----------------------------

-- ----------------------------
-- Table structure for product_images
-- ----------------------------
DROP TABLE IF EXISTS `product_images`;
CREATE TABLE `product_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_images_product_id_foreign` (`product_id`),
  CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product_images
-- ----------------------------

-- ----------------------------
-- Table structure for product_inventories
-- ----------------------------
DROP TABLE IF EXISTS `product_inventories`;
CREATE TABLE `product_inventories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qty` int(11) NOT NULL DEFAULT 0,
  `product_id` int(10) unsigned NOT NULL,
  `inventory_source_id` int(10) unsigned NOT NULL,
  `vendor_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_source_vendor_index_unique` (`product_id`,`inventory_source_id`,`vendor_id`),
  KEY `product_inventories_inventory_source_id_foreign` (`inventory_source_id`),
  CONSTRAINT `product_inventories_inventory_source_id_foreign` FOREIGN KEY (`inventory_source_id`) REFERENCES `inventory_sources` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_inventories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product_inventories
-- ----------------------------

-- ----------------------------
-- Table structure for product_ordered_inventories
-- ----------------------------
DROP TABLE IF EXISTS `product_ordered_inventories`;
CREATE TABLE `product_ordered_inventories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qty` int(11) NOT NULL DEFAULT 0,
  `product_id` int(10) unsigned NOT NULL,
  `channel_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_ordered_inventories_product_id_channel_id_unique` (`product_id`,`channel_id`),
  KEY `product_ordered_inventories_channel_id_foreign` (`channel_id`),
  CONSTRAINT `product_ordered_inventories_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_ordered_inventories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product_ordered_inventories
-- ----------------------------

-- ----------------------------
-- Table structure for product_relations
-- ----------------------------
DROP TABLE IF EXISTS `product_relations`;
CREATE TABLE `product_relations` (
  `parent_id` int(10) unsigned NOT NULL,
  `child_id` int(10) unsigned NOT NULL,
  KEY `product_relations_parent_id_foreign` (`parent_id`),
  KEY `product_relations_child_id_foreign` (`child_id`),
  CONSTRAINT `product_relations_child_id_foreign` FOREIGN KEY (`child_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_relations_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product_relations
-- ----------------------------

-- ----------------------------
-- Table structure for product_reviews
-- ----------------------------
DROP TABLE IF EXISTS `product_reviews`;
CREATE TABLE `product_reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `product_reviews_product_id_foreign` (`product_id`),
  KEY `product_reviews_customer_id_foreign` (`customer_id`),
  CONSTRAINT `product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product_reviews
-- ----------------------------

-- ----------------------------
-- Table structure for product_super_attributes
-- ----------------------------
DROP TABLE IF EXISTS `product_super_attributes`;
CREATE TABLE `product_super_attributes` (
  `product_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  KEY `product_super_attributes_product_id_foreign` (`product_id`),
  KEY `product_super_attributes_attribute_id_foreign` (`attribute_id`),
  CONSTRAINT `product_super_attributes_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`),
  CONSTRAINT `product_super_attributes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product_super_attributes
-- ----------------------------

-- ----------------------------
-- Table structure for product_up_sells
-- ----------------------------
DROP TABLE IF EXISTS `product_up_sells`;
CREATE TABLE `product_up_sells` (
  `parent_id` int(10) unsigned NOT NULL,
  `child_id` int(10) unsigned NOT NULL,
  KEY `product_up_sells_parent_id_foreign` (`parent_id`),
  KEY `product_up_sells_child_id_foreign` (`child_id`),
  CONSTRAINT `product_up_sells_child_id_foreign` FOREIGN KEY (`child_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_up_sells_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product_up_sells
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'Administrator', 'Administrator rolem', 'all', null, null, null);

-- ----------------------------
-- Table structure for shipments
-- ----------------------------
DROP TABLE IF EXISTS `shipments`;
CREATE TABLE `shipments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_qty` int(11) DEFAULT NULL,
  `total_weight` int(11) DEFAULT NULL,
  `carrier_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `carrier_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `track_number` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_sent` tinyint(1) NOT NULL DEFAULT 0,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `customer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `order_address_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `inventory_source_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shipments_order_id_foreign` (`order_id`),
  KEY `shipments_order_address_id_foreign` (`order_address_id`),
  KEY `shipments_inventory_source_id_foreign` (`inventory_source_id`),
  CONSTRAINT `shipments_inventory_source_id_foreign` FOREIGN KEY (`inventory_source_id`) REFERENCES `inventory_sources` (`id`) ON DELETE SET NULL,
  CONSTRAINT `shipments_order_address_id_foreign` FOREIGN KEY (`order_address_id`) REFERENCES `order_address` (`id`) ON DELETE SET NULL,
  CONSTRAINT `shipments_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of shipments
-- ----------------------------

-- ----------------------------
-- Table structure for shipment_items
-- ----------------------------
DROP TABLE IF EXISTS `shipment_items`;
CREATE TABLE `shipment_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `price` decimal(12,4) DEFAULT 0.0000,
  `base_price` decimal(12,4) DEFAULT 0.0000,
  `total` decimal(12,4) DEFAULT 0.0000,
  `base_total` decimal(12,4) DEFAULT 0.0000,
  `product_id` int(10) unsigned DEFAULT NULL,
  `product_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_item_id` int(10) unsigned DEFAULT NULL,
  `shipment_id` int(10) unsigned NOT NULL,
  `additional` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shipment_items_shipment_id_foreign` (`shipment_id`),
  CONSTRAINT `shipment_items_shipment_id_foreign` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of shipment_items
-- ----------------------------

-- ----------------------------
-- Table structure for sliders
-- ----------------------------
DROP TABLE IF EXISTS `sliders`;
CREATE TABLE `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sliders_channel_id_foreign` (`channel_id`),
  CONSTRAINT `sliders_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sliders
-- ----------------------------

-- ----------------------------
-- Table structure for subscribers_list
-- ----------------------------
DROP TABLE IF EXISTS `subscribers_list`;
CREATE TABLE `subscribers_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_subscribed` tinyint(1) NOT NULL DEFAULT 0,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subscribers_list_channel_id_foreign` (`channel_id`),
  CONSTRAINT `subscribers_list_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of subscribers_list
-- ----------------------------

-- ----------------------------
-- Table structure for tax_categories
-- ----------------------------
DROP TABLE IF EXISTS `tax_categories`;
CREATE TABLE `tax_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tax_categories_code_unique` (`code`),
  UNIQUE KEY `tax_categories_name_unique` (`name`),
  KEY `tax_categories_channel_id_foreign` (`channel_id`),
  CONSTRAINT `tax_categories_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tax_categories
-- ----------------------------

-- ----------------------------
-- Table structure for tax_categories_tax_rates
-- ----------------------------
DROP TABLE IF EXISTS `tax_categories_tax_rates`;
CREATE TABLE `tax_categories_tax_rates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tax_category_id` int(10) unsigned NOT NULL,
  `tax_rate_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tax_map_index_unique` (`tax_category_id`,`tax_rate_id`),
  KEY `tax_categories_tax_rates_tax_rate_id_foreign` (`tax_rate_id`),
  CONSTRAINT `tax_categories_tax_rates_tax_category_id_foreign` FOREIGN KEY (`tax_category_id`) REFERENCES `tax_categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tax_categories_tax_rates_tax_rate_id_foreign` FOREIGN KEY (`tax_rate_id`) REFERENCES `tax_rates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tax_categories_tax_rates
-- ----------------------------

-- ----------------------------
-- Table structure for tax_rates
-- ----------------------------
DROP TABLE IF EXISTS `tax_rates`;
CREATE TABLE `tax_rates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_zip` tinyint(1) NOT NULL DEFAULT 0,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate` decimal(12,4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tax_rates_identifier_unique` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tax_rates
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------

-- ----------------------------
-- Table structure for wishlist
-- ----------------------------
DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE `wishlist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `item_options` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `moved_to_cart` date DEFAULT NULL,
  `shared` tinyint(1) DEFAULT NULL,
  `time_of_moving` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wishlist_channel_id_foreign` (`channel_id`),
  KEY `wishlist_product_id_foreign` (`product_id`),
  KEY `wishlist_customer_id_foreign` (`customer_id`),
  CONSTRAINT `wishlist_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  CONSTRAINT `wishlist_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  CONSTRAINT `wishlist_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of wishlist
-- ----------------------------
